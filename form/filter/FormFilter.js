define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-class",
    "doze/form/filter/_Filter",
    "dojo/text!./templates/FormFilter.html"
], function (declare, lang, cls, _Filter, template) {

    return declare("doze.form.filter.FormFilter",
            [_Filter],
            {
                templateString: template,
                labelClass: '',
                itemType: 'text',
                label: '',
                postCreate: function () {
               //     this._setupValueWidgets();
                    this.labelNode.innerHTML = this.label;
                    cls.add(this.labelNode, this.labelClass);
                },
				/*
                _setupValueWidgets: function () {
                    if (this.itemValueCount >= 1) {
                        this._getValueWidget(this.valueNode, this.itemType, this.fieldValue);
                    }
                    if (this.itemValueCount >= 2) {
                        this._getValueWidget2(this.valueNode, this.itemType, this.fieldValue);
                    }
                },
				*/
                _getValueAttr: function () {
                    var rep = [];

                    if (this.itemValueCount >= 1 && !this.valueWidget)
                        return null;
                    if (this.itemValueCount >= 2 && !this.valueWidget2)
                        return null;

                    return this.inherited(arguments);
                },
                serialize: function () {
                    var obj = this.inherited(arguments);
                    lang.setObject('label', this.label, obj);
                    lang.setObject('labelClass', this.labelClass, obj);
                    lang.setObject('itemType', this.itemType, obj);
                    return obj;
                }
            });
});
