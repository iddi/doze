define("doze/form/filter/Hidden",
        ["dojo/_base/declare", "dojo", "doze/form/filter/_Filter"],
        function (declare, dojo, _Filter) {

            return declare("doze.form.filter.Hidden",
                    [_Filter],
                    {
                        postCreate: function () {
                            dojo.style(this.domNode, 'display', 'none');
                        }
                    });
        });
