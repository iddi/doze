define(
        [
            "dojo/_base/declare",
            "dojo/dom-class",
            "dijit/form/Select",
            "dojo",
            "dojo/on",
            "dojo/_base/lang",
            "doze/form/filter/_Filter",
            "dojo/text!./templates/Operator.html",
            "dojo/i18n!../nls/filter"
        ], function (declare, cssClass, Select, dojo, on, lang, _Filter, template, i18n) {


    return declare("doze.form.filter.Operator",
            [_Filter],
            {
                templateString: template,
                i18n: i18n,
                operatorCombo: null,
                originalValues: null,
                label: '',
                definition: null,
                postCreate: function () {
                    on(this.buttonNode, "click", lang.hitch(this, this.onRemove));
                    this.operatorCombo = new Select({
                        labelAttr: 'name',
                        query: {type: this.definition.type},
                        queryOptions: {sort: [{attribute: "order"}]},
                        onSetStore: lang.hitch(this, this.onOperatorChange),
                        sortByLabel: false
                    }, this.operatorNode);
                    this.operatorCombo.setStore(this.storeDefinitions.getOperatorStore());

                    this.operatorCombo.on('change', lang.hitch(this, this.onOperatorChange));
                    cssClass.add(this.domNode, 'filter-field-' + this.definition.fieldname);
                },
                onOperatorChange: function () {
                    if (this.defferedOperatorValue) {
                        this.operatorCombo.set('value', this.operatorValue);
                        delete this.defferedOperatorValue;
                    }
                    if (this.operatorCombo.get('value')) {
                        var opItem = this.operatorCombo.store.get(this.operatorCombo.get('value'));
                        this.itemValueCount = opItem.valuecount;
                        if (!this.valueWidget)
                            this._getValueWidget(this.valueNode, this.definition);
  //                          this._getValueWidget(this.valueNode, this.definition.type, this.definition.fieldname);
                        if (this.itemValueCount === 2) {
                            if (!this.valueWidget2)
                                this._getValueWidget2(this.valueNode2, this.definition);
//                                this._getValueWidget2(this.valueNode2, this.definition.type, this.definition.fieldname);
                        } else if (this.valueWidget2) {
                            this.valueWidget2.destroy();
                            delete this.valueWidget2;
                        }
                    }

                },
                _getValueAttr: function () {
                    if (!this.valueWidget && this.itemValueCount !== 0)
                        return this.originalValues;
                    if (this.itemValueCount === 2 && !this.valueWidget2)
                        return this.originalValues;

                    this.fieldValue = this.definition.fieldname;
                    this.operatorValue = this.operatorCombo.get('value');
                    return this.inherited(arguments);
                },
                _setValueAttr: function (/* Array */values) {

                    try {
                        this.originalValues = dojo.clone(values);
                        this.defferedValues = values;
                        this.defferedValues.shift();
                        this.operatorValue = this.defferedValues.shift();
                        if (this.operatorCombo) {
                            this.operatorCombo.set('value', this.operatorValue);
                        } else {
                            this.defferedOperatorValue = this.operatorValue;
                        }
                    }
                    catch (E) {
                    }

                },
                serialize: function () {
                    var obj = this.inherited(arguments);
                    dojo.setObject('definition', this.definition, obj);
                    dojo.setObject('label', this.label, obj);
                    return obj;
                }
            });
});
