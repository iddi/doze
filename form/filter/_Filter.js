define([
    "dojo/_base/declare",
    "dijit",
    "dojo",
    "dojo/keys",
    "dojo/Evented",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/store/Memory",
    "dijit/form/NumberTextBox",
    "dijit/form/CheckBox",
    "dojox/form/CheckedMultiSelect"
], function (declare, dijit, dojo, keys, Evented,
        _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
        Memory
        ) {

    return declare("doze.form.filter._Filter",
            [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented],
            {
                sourceName: '',
                valuesData: null,
                valuePrefetch: false,
                valueWidget: null,
                valueWidget2: null,
                valueClass: '',
                dateOptions: {datePattern: "yyyy-MM-dd", selector: "date"},
                defferedValues: null,
                fieldValue: '',
                required: false,
                operatorValue: 0,
                itemValueCount: 1,
                widgetValue: null,
                widget2Value: null,
                value: [],
                selectFirst: false,
                _selectFirstValue: false,
                challengeText: '',
                _searchTimeoutId: null,
                searchTimeout: 300,
                searchMinLength: 1,
                searchIfEmpty: true,
                storeDefinitions: null,
                onRemove: function () {
                    this.emit("remove", {});
                },
                onChange: function () {
                    this.emit("change", {});
                },
                _getValueWidget: function (node, definition) {
				console.log(definition);
				t=this;
				
					var itemType= definition.type;
					var identity= definition.fieldname
				
				
                    if (this.valueWidget) {
                        this.valueWidget.destroy();
                        delete this.valueWidget;
                    }

                    if (this.valueWidget2) {
                        this.valueWidget2.destroy();
                        delete this.valueWidget2;
                    }
                    var wdgDiv = dojo.create('div');
                    node.appendChild(wdgDiv);

                    if (/^text$/.test(itemType)) {
                        this.valueWidget = new dijit.form.ValidationTextBox({
                            required: this.required,
                            "class": this.valueClass,
                            "type": "search"
                        }, wdgDiv);
                    }
                    if (/fulltext/.test(itemType)) {
                        this.valueWidget = new dijit.form.ValidationTextBox({
                            required: this.required,
                            "class": this.valueClass,
                            "type": "search",
                            "title": "YOU:* - YOUNG & OLD - YOUNG | OLD - YOUNG & OL:*"
                        }, wdgDiv);
                    }

                    if (/boolean/.test(itemType)) {
                        this.valueWidget = null;
                    }
                    if (/date/.test(itemType) || /datetime/.test(itemType)) {
                        this.valueWidget = new dijit.form.DateTextBox({
                            required: this.required,
                            "class": this.valueClass,
                            constraints: {datePattern: 'dd/MM/yyyy'}
                        }, wdgDiv);
                    }
                    if (/number/.test(itemType)) {
                        this.valueWidget = new dijit.form.NumberTextBox({
                            required: this.required,
                            "class": this.valueClass,
                            "constraints": {pattern: "#"}
                        }, wdgDiv);

                    }

                    if (/select/.test(itemType)) {
                        var def = this.storeDefinitions.getValueStoreDefinition(identity);
                        this.valueStore = def.store;
t=this;
						console.log(this);
						
                        this.valueWidget = new dijit.form.FilteringSelect({
                            searchDelay: 300,
                            store: this.valueStore,
                            "class": this.valueClass,
                            searchAttr: def.searchAttr,
                            labelAttr: def.searchAttr,
                            valueField: def.idProperty,
							queryExpr: definition.queryExpr?definition.queryExpr:"${0}*",							
                            autoComplete: false,
                            required: this.required,
                            pageSize: 20
                        }, wdgDiv);

                    }

                    if (/listbox/.test(itemType)) {

                        if (this.valuesData) {
                            /*
                             this.valueStore = new dojo.data.ItemFileReadStore({
                             data: this.valuesData
                             });
                             */
                            this.valueStore = new Memory({data: this.valuesData});
                        } else {
                            this.valueStore = this.storeDefinitions.getValueStore(this.sourceName);
                            /*
                             this.valueStore = new dojo.data.ItemFileReadStore({
                             url: doze.formatUrl({
                             module: this.moduleName,
                             controller: this.controllerName,
                             action: 'source',
                             source: this.sourceName,
                             id: identity
                             })
                             });
                             */
                        }
                        //this.valueStore.fetch();
                        this.valueWidget = new dijit.form.Select({
                            searchDelay: 300,
                            "class": this.valueClass,
                            store: this.valueStore,
                            required: this.required,
                            pageSize: 20
                        }, wdgDiv);
                        //this.valueWidget.setStore(this.valueStore);
//                        this.connect(this.valueWidget, 'onChange', 'onFilter');
                    }

                    if (/multiple/.test(itemType)) {
                        if (this.valuesData) {
                            /*
                             this.valueStore = new dojo.data.ItemFileReadStore({
                             data: this.valuesData
                             });
                             */
                            this.valueStore = new Memory({data: this.valuesData});
                        } else {
                            this.valueStore = this.storeDefinitions.getValueStore(this.sourceName);
                            /*
                             this.valueStore = new dojo.data.ItemFileReadStore({
                             url: doze.formatUrl({
                             module: this.moduleName,
                             controller: this.controllerName,
                             action: 'source',
                             source: this.sourceName,
                             id: identity
                             })
                             });
                             */
                        }
                        /*
                         var args = doze.config.multifilterUrlArgs;
                         args.action = 'source';
                         args.source = this.sourceName;
                         args.id = identity;
                         
                         this.valueStore = new dojo.data.ItemFileReadStore({
                         url:doze.formatUrl(args)
                         });
                         */
                        //this.valueStore.fetch();

                        this.valueWidget = new dojox.form.CheckedMultiSelect({
                            "class": this.valueClass,
                            store: this.valueStore,
                            multiple: true,
                            size: 2
                        }, wdgDiv);
                        //this.valueWidget.setStore(this.valueStore);
//                        this.connect(this.valueWidget, 'onChange', 'onFilter');
                    }

                    this.connect(this.valueWidget, 'onKeyPress', '_onKeyPress');

                    if (this.defferedValues && this.defferedValues.length > 0) {
                        this._setWidgetValue(this.valueWidget, this.defferedValues.shift());
                    } else {
                        if (this.valueWidget)
                            this.valueWidget.focus();
                    }


                },
                _onKeyPress: function (evt) {
                    if ((evt.charCode || evt.keyCode) === keys.ENTER) {
                        evt.cancelBubble = true;
                        if (evt.stopPropagation)
                            evt.stopPropagation();
                        this.emit("apply", {});

                    }
                },
				_getValueWidget2: function (node, definition) {
				
					var itemType=this.definition.type;
					var identity= this.definition.fieldname

                    if (this.valueWidget2) {
                        this.valueWidget2.destroy();
                        delete this.valueWidget2;
                    }
                    var makeFocus = true;
                    var wdgDiv = dojo.create('div');
                    node.appendChild(wdgDiv);
                    if (/date/.test(itemType) || /datetime/.test(itemType)) {
                        this.valueWidget2 = new dijit.form.DateTextBox({
                            "required": this.required,
                            "class": this.valueClass,
                            "constraints": {datePattern: 'dd/MM/yyyy'}
                        }, wdgDiv);
                    }
                    if (/number/.test(itemType)) {
                        this.valueWidget2 = new dijit.form.NumberTextBox({
                            required: this.required,
                            "class": this.valueClass,
                            "constraints": {pattern: "#"}
                        }, wdgDiv);
                    }
                    dojo.connect(this.valueWidget, 'onChange', this, function () {
                        if (this.valueWidget2)
                            this.valueWidget2.constraints.min = this.valueWidget.get('value');
                    });
                    dojo.connect(this.valueWidget2, 'onChange', this, function () {
                        this.valueWidget.constraints.max = this.valueWidget2.get('value');
                    });

                    if (this.defferedValues && this.defferedValues.length > 0) {
                        makeFocus = false;
                        this._setWidgetValue(this.valueWidget2, this.defferedValues.shift());
                    }

                    if (this.valueWidget && makeFocus && !this.defferedValues && this.defferedValues.length == 0) {
                        this.valueWidget.focus();
                    }
                },
                _getWidgetValue: function (wdg) {
                    var val = (wdg) ? ((wdg.dateLocaleModule && wdg.get('value')) ? wdg.dateLocaleModule.format(wdg.get('value'), this.dateOptions) : wdg.get('value')) : '';
                    return val;
                },
                _setWidgetValue: function (wdg, value) {
                    if (wdg)
                        wdg.set('value', ((wdg.dateLocaleModule && value) ? wdg.dateLocaleModule.parse(value, this.dateOptions) : value));
                },
                _getValueAttr: function () {
                    var rep = [];
                    rep.push(this.fieldValue);
                    rep.push(this.operatorValue);
                    if (this.itemValueCount === 0)
                        rep.push('bool');
                    if (this.itemValueCount >= 1)
                        rep.push(((this.valueWidget) ? this._getWidgetValue(this.valueWidget) : this.widgetValue));
                    if (this.itemValueCount >= 2)
                        rep.push(((this.valueWidget2) ? this._getWidgetValue(this.valueWidget2) : this.widget2Value));
                    return rep;
                },
                _getDisplayedValueAttr: function () {
                    var rep = [];
                    if (this.itemValueCount >= 1)
                        rep.push(((this.valueWidget) ? this.valueWidget.get('displayedValue') : null));
                    if (this.itemValueCount >= 2)
                        rep.push(((this.valueWidget2) ? this.valueWidget2.get('displayedValue') : null));
                    return rep;
                },
                serialize: function () {
                    var obj = {};
                    dojo.setObject('value', this.get('value'), obj);
                    dojo.setObject('declaredClass', this.declaredClass, obj);
                    dojo.setObject('itemValueCount', this.itemValueCount, obj);
                    dojo.setObject('valueClass', this.valueClass, obj);
                    dojo.setObject('required', this.required, obj);
                    dojo.setObject('challengeText', this.challengeText, obj);
                    dojo.setObject('operatorLabel', this.operatorCombo.get('selectedOptions') ? this.operatorCombo.get('selectedOptions').label : '', obj);
                    dojo.setObject('displayedValues', this.get('displayedValue'), obj);
                    return obj;
                },
                _getFielditemAttr: function () {
                    return null;
                },
                _setValueAttr: function (/* Array */values) {
                    try {
                        this.defferedValues = values;
                        this.fieldValue = this.defferedValues.shift();
                        this.operatorValue = this.defferedValues.shift();
                        if (this.defferedValues.length > 0) {
                            this.widgetValue = this.defferedValues[0];
                            if (this.valueWidget)
                                this._setWidgetValue(this.valueWidget, this.defferedValues.shift());
                        }
                        if (this.defferedValues.length > 0) {
                            this.widget2Value = this.defferedValues[0];
                            if (this.valueWidget2)
                                this._setWidgetValue(this.valueWidget, this.defferedValues.shift());
                        }
                    }
                    catch (E) {
                        if (window.console) {
                            console.log(E);
                        }
                    }
                },
                selectFirstValue: function () {

                    this._selectFirstValue = true;
                    this.fieldValue = null;

                    this.fieldStore.fetch({
                        onItem: dojo.hitch(this,
                                function (it) {
                                    if (!this.fieldValue) {
                                        this.fieldValue = this.fieldStore.getIdentity(it);
                                    }
                                })
                    });
                }
            });
});
