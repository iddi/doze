define(
        [
            "dojo/_base/declare",
            "dojo/query",
            "dojo/on",
            "dojo/text!doze/form/resources/Uploader.html",
            "dojo/dom-style",
            "dojo/store/JsonRest",
            "dijit/form/_FormValueWidget",
            "dijit/_WidgetsInTemplateMixin",
            "dojox/form/Uploader",
            "dijit/form/Button",
            "dijit/Tooltip",
            "dojo/mouse"

        ], function (declare,
        query,
        on,
        UploaderTemplate,
        domStyle,
        JsonRest,
        _FormValueWidget,
        WidgetsInTemplateMixin,
        Uploader,
        Button,
        Tooltip,
        mouse) {
    return declare("doze.form.Uploader", [_FormValueWidget, WidgetsInTemplateMixin], {
        templateString: UploaderTemplate,
        baseClass: "dozeUploader",
        // disabled: boolean
        //		Whether or not this widget is disabled
        disabled: false,
        // readOnly: boolean
        //		Whether or not this widget is readOnly
        readOnly: false,
        // required: Boolean
        //		User is required to check at least one item.
        required: false,
        // invalidMessage: String
        //		The message to display if value is invalid.
        invalidMessage: "You must upload a file", // "$_unset_$", // TODO nls

        // _message: String
        //		Currently displayed message
        _message: "",
        // 
        // upload_url: string
        //		Indicate the upload url
        upload_url: '',
        // TODO: il faudrait que les infos (comme l'url, le nom du fichier, ...) viennent avec set value
        // info_url: string
        //		Indicate the info url
        info_url: '',
        //_startStyle: '',
        //_btnNode: null,
        postCreate: function () {
            this.inherited(arguments);
            var _this = this;
            //this._btnNode = query('.dijitButtonNode', this.uploader.domNode)[0];
            //this._startStyle = domStyle.get(this._btnNode, 'background');

            this.uploader.set('url', this.upload_url);

            on(this.uploader, "complete", function (items) {
                _this.uploader.set("label", "Replace file");
                //var btn = query('.dijitButtonNode', _this.uploader.domNode)[0];
                //  domStyle.set(btn, 'background', _startStyle);
                if (items.length > 0) {
                    Tooltip.show("upload ok", _this.uploader.domNode);
                    on.once(_this.uploader.domNode, mouse.leave, function () {
                        Tooltip.hide(_this.uploader.domNode);
                    });
                } else {
                    var message = "<table><tr><td><span class='tooltipIcon icon-dozeexclamation-sign'>&nbsp;</span></td><td  class='tooltipMessage'>Upload failed.</td></tr></table>"
                    Tooltip.show(message, _this.uploader.domNode);
                    on.once(_this.uploader.domNode, mouse.leave, function () {
                        Tooltip.hide(_this.uploader.domNode);
                    });
                    return;
                }
                _this.set('value', items[0].id_file_uploaded);
                // TODO mettre le nom dans l'info
                _this.textNode.innerHTML = '<a href="rest/inscriptions/inscriptions.xls/12/view?file=' + items[0].filename_system + '"> ' + items[0].filename_original + '</a>';
//                _this.controller.model.cv = items[0].id_file_uploaded;
//                _this._setDirtyAttr(true);
                domStyle.set(_this.deleteButton.domNode, 'display', 'inline-block');


            });

            on(this.uploader, "begin", function (progress) {
                _this.uploader.set('label', 'Uploading...');
            });
            on(this.uploader, "progress", function (progress) {
//                                console.log('progress');
                //bytesLoaded: 75235328
                //bytesTotal: 102875827
                //decimal: 0.7313217321694045
                //percent: "74%"
                //timeStamp: 1384437239763
                //type: "progress"
                var _percent = Math.round(progress.decimal * 100);
                // var _secondStop = (_percent < 95) ? _percent + 5 : 100;
                //console.log(_getStyle(_percent, _secondStop));
                if (_percent < 100) {
                    _this.uploader.set('label', 'Uploading ' + _percent + '% ...');
                }

                //domStyle.set(_this._btnNode, 'background', _this._getCssValuePrefix('background', _this._getStyle(_percent, _secondStop)) + _getStyle(_percent, _secondStop));

            });
            on(this.uploader, "error", function () {
                console.log(arguments);
                //domStyle.set(_this._btnNode, 'background', _startStyle);
                _this.uploader.set("label", "Replace file");

                //TODO: emit a doze error???
                var message = "<table><tr><td><span class='tooltipIcon icon-dozeexclamation-sign'>&nbsp;</span></td><td  class='tooltipMessage'>Upload failed.</td></tr></table>";
                Tooltip.show(message, _this.uploader.domNode);
                on.once(_this.uploader.domNode, mouse.leave, function () {
                    Tooltip.hide(_this.uploader.domNode);
                });
            });
        },
        clear_file: function () {
            this.set('value', '');
        },
        validator: function () {
            // summary:
            //		Overridable function used to validate that an item is selected if required =
            //		true.
            // tags:
            //		protected
            if (!this.required) {
                return true;
            }
            return (this.get('value') != '')
        },
        validate: function (isFocused) {
            Tooltip.hide(this.domNode);
            var isValid = this.isValid(isFocused);
            if (!isValid) {
                this.displayMessage(this.invalidMessage);
            }
            return isValid;
        },
        isValid: function (/*Boolean*/ isFocused) {
            // summary:
            //		Tests if the required items are selected.
            //		Can override with your own routine in a subclass.
            // tags:
            //		protected
            return this.validator();
        },
        getErrorMessage: function (/*Boolean*/ isFocused) {
            // summary:
            //		Return an error message to show if appropriate
            // tags:
            //		protected
            return this.invalidMessage;
        },
        displayMessage: function (/*String*/ message) {
            // summary:
            //		Overridable method to display validation errors/hints.
            //		By default uses a tooltip.
            // tags:
            //		extension
            Tooltip.hide(this.domNode);
            if (message) {
                Tooltip.show(message, this.domNode, this.tooltipPosition);
            }
        },
        _refreshState: function () {
            // summary:
            //		Validate if selection changes.
            this.validate(this.focused);
        },
        onChange: function (newValue) {
            // summary:
            //		Validate if selection changes.
            this._refreshState();
        },
        reset: function () {
            // Overridden so that the state will be cleared.
            this.inherited(arguments);
            Tooltip.hide(this.domNode);
        },
        _setDisabledAttr: function (value) {
            // summary:
            //		Disables (or enables) all the children as well
            this.disabled = value;
            this.uploader.set("disabled", this.disabled);
            this.deleteButton.set("disabled", this.disabled);
            //domClass.toggle(this.domNode, "dojoxMultiSelectDisabled", this.disabled);
        },
        _setReadOnlyAttr: function (value) {
            // summary:
            //		Sets read only (or unsets) all the children as well
            if (value) {
                if (this.value) {
                    domStyle.set(this.deleteButton.domNode, 'display', 'inline-block');
                }
                domStyle.set(this.uploader.domNode, 'display', 'inline-block');
            } else {
                domStyle.set(this.deleteButton.domNode, 'display', 'none');
                domStyle.set(this.uploader.domNode, 'display', 'none');

            }
            this.readOnly = value;
        },
        _setValueAttr: function (value) {

            var _this = this;
            if (value) {
                // TODO mettre le nom dans value pour éviter de devoir refaire une query pour avoir les info

                var XhrStore = new JsonRest({
                    target: this.info_url + value,
                    idProperty: 'id_file_uploaded',
                    headers: {
                        'X-DoZe-Search': 'simple'
                    }});

                XhrStore.query({}).then(function (items) {
                    _this.textNode.innerHTML =
                            '<a href="rest/inscriptions/inscriptions.xls/12/view?file=' + items.filename_system + '"> ' + items.filename_original + '</a>';
                    _this.uploader.set("label", "Replace file");
                    if (!this.readOnly) {
                        domStyle.set(_this.deleteButton.domNode, 'display', 'inline-block');
                    }
                });
            } else {
                _this.uploader.set("label", "Upload File");
                _this.textNode.innerHTML = '';
                domStyle.set(_this.deleteButton.domNode, 'display', 'none');
            }
            // summary:
            //		Hook so set('value', value) works.
            /*            this.value= value;
             this.valueNode.value = value;
             this.focusNode.setAttribute("aria-valuenow", value);
             */
            this.inherited(arguments);
        }
        /*,
         _getCssValuePrefix: function(name, value) {
         
         var prefixes = ['', '-o-', '-ms-', '-moz-', '-webkit-'];
         // Create a temporary DOM object for testing
         var dom = document.createElement('div');
         for (var i = 0; i < prefixes.length; i++) {
         // Attempt to set the style
         dom.style[name] = prefixes[i] + value;
         // Detect if the style was successfully set
         if (dom.style[name]) {
         return prefixes[i];
         }
         dom.style[name] = ''; // Reset the style
         }
         },
         _getStyle: function(firstStop, secondStop) {
         return 'linear-gradient( 90deg , #59A75C 0%, #59A75C ' + firstStop + '%, #5972A7 ' + secondStop + '%, #5972A7 100%)';
         }*/

    });
});


