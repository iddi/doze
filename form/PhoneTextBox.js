define(
        ["dojo", "dijit", "dijit/form/ValidationTextBox"],
        function (dojo, dijit) {


            dojo.declare("doze.form.PhoneTextBox", [dijit.form.ValidationTextBox],
                    {
                        // summary:
                        //		summary
                        //
                        // description:
                        //		description

                        regExp: '\\+[ 0-9]*', /* '[0-9]{11}' */
                        regExp_internal_extension: '[0-9]{4}', /* '[0-9]{11}' */

                        nationality: 'BE',
                        required: false,
                        allow_internal_extension: true,
                        filter: function (val) {
                            val = this.inherited(arguments);
                            val = val.replace(/[^ +0-9]/g, " "); // remove unespacted characters
                            val = val.replace(/\s{2,}/g, " "); // replace multispace into one space
                            return val;
                        },
                        validator: function (value, constraints) {
                            value = this.filter(value);

                            if (this._isEmpty(value) && (!this.required)) {
                                return true;
                            }
                            if ((((new RegExp("^(?:" + this.regExp_internal_extension + ")" + (this.required ? "" : "?") + "$")).test(value)) == true)
                                    && (this.allow_internal_extension)) {
                                return true;
                            }

                            if (((new RegExp("^(?:" + this.regExpGen(constraints) + ")" + (this.required ? "" : "?") + "$")).test(value)) == false) {
                                this.invalidMessage = 'Format invalide. Le format attendu est soit une extension interne à 4 chiffres soit un numéro de téléphone belge sous format international (+32 2 222 22 22)';
                                return false;
                            }

                            if (value.length < 10) {
                                this.invalidMessage = 'Numéro trop court. Le format attendu est soit une extension interne à 4 chiffres soit un numéro de téléphone belge sous format international (+32 2 222 22 22)';
                                return false;

                            }
                            if ((this.nationality == 'BE') && (value.substring(0, 3) != '+32')) {
                                this.invalidMessage = 'Uniquement les numéros belges sont autorisés. Le format attendu est soit une extension interne à 4 chiffres soit un numéro de téléphone belge sous format international (+32 2 222 22 22)';
                                return false;
                            }

                            return true;
                            /*
                             } else {
                             if (((new RegExp("^(?:" + this.regExpGen(constraints) + ")"+(this.required?"":"?")+"$")).test(value)) == false) {
                             this.invalidMessage = 'Invalid format. Expected format : nnnnnnnnnnn';
                             return false;
                             }
                             return true;
                             }*/
                        }

                    });
            return amster.form.PhoneTextBox;

        });
