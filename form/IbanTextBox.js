//>>built
define("amster/form/IbanTextBox",
        ["dojo", "dijit", "dijit/form/ValidationTextBox"],
        function (dojo, dijit) {

            //TODO utiliser  le MaskedEditTextBox: masque:AA99 **** **** **** **** **** **** **
            //TODO add a list_of_accepted_countries + possibility EUROZONE, EU, EUROPE, ALL     

            dojo.declare("doze.form.IbanTextBox", [dijit.form.ValidationTextBox],
                    {
                        // summary:
                        //		summary
                        //
                        // description:
                        //		description

                        regExp: '[A-Z]{2}[0-9]{2}[A-Z0-9]{4}[0-9]{7}([A-Z0-9]?){0,16}',
                        uppercase: true,
                        trim: true,
                        country: '',
                        requiredIBANCountry: false,
                        ibanexp: {
                            'AD': {
                                name: "Andorra",
                                l: 24,
                                f: "F04F04A12",
                                t1: "n",
                                t2: "n",
                                euro: true,
                                eg: "AD1200012030200359100100",
                                regex: '^(AD)[0-9]{2}[0-9]{8}[A-Z0-9]{12}$'
                            },
                            'BE': {
                                name: "Belgium",
                                l: 16,
                                f: "F03F07F02",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "BE68539007547034",
                                regex: '^(BE)[0-9]{2}[0-9]{3}[0-9]{9}$'
                            },
                            'BG': {
                                name: "Bulgaria",
                                l: 22,
                                f: "U04F04F02A08",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "BG80BNBG96611020345678",
                                regex: '^(BG)[0-9]{2}[A-Z]{4}[0-9]{4}[0-9]{2}[A-Z0-9]{8}$'
                            },
                            'CH': {
                                name: "Switzerland",
                                l: 21,
                                f: "F05A12",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "CH9300762011623852957",
                                regex: '^(CH)[0-9]{2}[0-9]{5}[A-Z0-9]{12}$'
                            },
                            'RS': {
                                name: "Serbia",
                                l: 22,
                                f: "F03F13F02",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "RS35260005601001611379",
                                regex: '^(RS)[0-9]{2}[0-9]{3}[0-9]{15}$'
                            },
                            'CY': {
                                name: "Cyprus",
                                l: 28,
                                f: "F03F05A16",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "CY17002001280000001200527600",
                                regex: '^(CY)[0-9]{2}[0-9]{8}[A-Z0-9]{16}$'
                            },
                            'CZ': {
                                name: "Czech Republic",
                                l: 24,
                                f: "F04F06F10",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "CZ6508000000192000145399",
                                regex: '^(CZ)[0-9]{2}[0-9]{4}[0-9]{16}$'
                            },
                            'DE': {
                                name: "Germany",
                                l: 22,
                                f: "F08F10",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "DE89370400440532013000",
                                regex: '^(DE)[0-9]{2}[0-9]{8}[0-9]{10}$'
                            },
                            'DK': {
                                name: "Denmark",
                                l: 18,
                                f: "F04F09F01",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "DK5000400440116243",
                                regex: '^(DK)[0-9]{2}[0-9]{4}[0-9]{10}$'
                            },
                            'EE': {
                                name: "Estonia",
                                l: 20,
                                f: "F02F02F11F01",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "EE382200221020145685",
                                regex: '^(EE)[0-9]{2}[0-9]{4}[0-9]{12}$'
                            },
                            'ES': {
                                name: "Spain",
                                l: 24,
                                f: "F04F04F01F01F10",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "ES9121000418450200051332",
                                regex: '^(ES)[0-9]{2}[0-9]{8}[0-9]{12}$'
                            },
                            'FR': {
                                name: "France",
                                l: 27,
                                f: "F05F05A11F02",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "FR1420041010050500013M02606",
                                regex: '^(FR)[0-9]{2}[0-9]{10}[A-Z0-9]{13}$'
                            },
                            'FI': {
                                name: "Finland",
                                l: 18,
                                f: "F06F07F01",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "FI2112345600000785",
                                regex: '^(FI)[0-9]{2}[0-9]{6}[0-9]{8}$'
                            },
                            'GB': {
                                name: "United Kingdom",
                                l: 22,
                                f: "U04F06F08",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "GB29NWBK60161331926819",
                                regex: '^(GB)[0-9]{2}[A-Z]{4}[0-9]{14}$'
                            },
                            'GI': {
                                name: "Gibraltar",
                                l: 23,
                                f: "U04A15",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "GI75NWBK000000007099453",
                                regex: '^(GI)[0-9]{2}[A-Z]{4}[A-Z0-9]{15}$'
                            },
                            'HR': {
                                name: "Croatia",
                                l: 21,
                                f: "F07F10",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "HR1210010051863000160",
                                regex: '^(HR)[0-9]{2}[0-9]{7}[0-9]{10}$'
                            },
                            'HU': {
                                name: "Hungary",
                                l: 28,
                                f: "F03F04F01F15F01",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "HU42117730161111101800000000",
                                regex: '^(HU)[0-9]{2}[0-9]{7}[0-9]{1}[0-9]{15}[0-9]{1}$'
                            },
                            'IE': {
                                name: "Ireland",
                                l: 22,
                                f: "U04F06F08",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "IE29AIBK93115212345678",
                                regex: '^(IE)[0-9]{2}[A-Z0-9]{4}[0-9]{6}[0-9]{8}$'
                            },
                            'IS': {
                                name: "Iceland",
                                l: 26,
                                f: "F04F02F06F10",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "IS140159260076545510730339",
                                regex: '^(IS)[0-9]{2}[0-9]{4}[0-9]{18}$'
                            },
                            'LT': {
                                name: "Lithuania",
                                l: 20,
                                f: "F05F11",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "LT121000011101001000",
                                regex: '^(LT)[0-9]{2}[0-9]{5}[0-9]{11}$'
                            },
                            'LV': {
                                name: "Latvia",
                                l: 21,
                                f: "U04A13",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "LV80BANK0000435195001",
                                regex: '^(LV)[0-9]{2}[A-Z]{4}[A-Z0-9]{13}$'
                            },
                            'MK': {
                                name: "Macedonia",
                                l: 19,
                                f: "F03A10F02",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "MK07250120000058984",
                                regex: '^(MK)[0-9]{2}[A-Z]{3}[A-Z0-9]{10}[0-9]{2}$'
                            },
                            'MT': {
                                name: "Malta",
                                l: 31,
                                f: "U04F05A18",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "MT84MALT011000012345MTLCAST001S",
                                regex: '^(MT)[0-9]{2}[A-Z]{4}[0-9]{5}[A-Z0-9]{18}$'
                            },
                            'NO': {
                                name: "Norway",
                                l: 15,
                                f: "F04F06F01",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "NO9386011117947",
                                regex: '^(NO)[0-9]{2}[0-9]{4}[0-9]{7}$'
                            },
                            'PL': {
                                name: "Poland",
                                l: 28,
                                f: "F08F16",
                                t1: "y",
                                t2: "y",
                                euro: false,
                                eg: "PL27114020040000300201355387",
                                regex: '^(PL)[0-9]{2}[0-9]{8}[0-9]{16}$'
                            },
                            'PT': {
                                name: "Portugal",
                                l: 25,
                                f: "F04F04F11F02",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "PT50000201231234567890154",
                                regex: '^(PT)[0-9]{2}[0-9]{8}[0-9]{13}$'
                            },
                            'SE': {
                                name: "Sweden",
                                l: 24,
                                f: "F03F16F01",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "SE4550000000058398257466",
                                regex: '^(SE)[0-9]{2}[0-9]{3}[0-9]{17}$'
                            },
                            'SI': {
                                name: "Slovenia",
                                l: 19,
                                f: "F05F08F02",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "SI56191000000123438",
                                regex: '^(SI)[0-9]{2}[0-9]{5}[0-9]{8}[0-9]{2}$'
                            },
                            'SK': {
                                name: "Slovak Republic",
                                l: 24,
                                f: "F04F06F10",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "SK3112000000198742637541",
                                regex: '^(SK)[0-9]{2}[0-9]{4}[0-9]{16}$'
                            },
                            'AT': {
                                name: "Austria",
                                l: 20,
                                f: "F05F11",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "AT611904300234573201",
                                regex: '^(AT)[0-9]{2}[0-9]{5}[0-9]{11}$'
                            },
                            'GR': {
                                name: "Greece",
                                l: 27,
                                f: "F03F04A16",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "GR1601101250000000012300695",
                                regex: '^(GR)[0-9]{2}[0-9]{7}[A-Z0-9]{16}$'
                            },
                            'NL': {
                                name: "Netherlands",
                                l: 18,
                                f: "U04F10",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "NL91ABNA0417164300",
                                regex: '^(NL)[0-9]{2}[A-Z]{4}[0-9]{10}$'
                            },
                            'IT': {
                                name: "Italy",
                                l: 27,
                                f: "U01F05F05A12",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "IT60X0542811101000000123456",
                                regex: '^(IT)[0-9]{2}[A-Z]{1}[0-9]{10}[A-Z0-9]{12}$'
                            },
                            'LI': {
                                name: "Liechtenstein",
                                l: 21,
                                f: "F05A12",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "LI21088100002324013AA",
                                regex: '^(LI)[0-9]{2}[0-9]{5}[A-Z0-9]{12}$'
                            },
                            'LU': {
                                name: "Luxembourg",
                                l: 20,
                                f: "F03A13",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "LU280019400644750000",
                                regex: '^(LU)[0-9]{2}[0-9]{3}[A-Z0-9]{13}$'
                            },
                            'MC': {
                                name: "Monaco",
                                l: 27,
                                f: "F05F05A11F02",
                                t1: "y",
                                t2: "n",
                                euro: true,
                                eg: "MC5811222000010123456789030",
                                regex: '^(MC)[0-9]{2}[0-9]{5}[0-9]{5}[A-Z0-9]{11}[0-9]{2}$'
                            },
                            'ME': {
                                name: "Montenegro",
                                l: 22,
                                f: "F03F13F02",
                                t1: "n",
                                t2: "n",
                                euro: true,
                                eg: "ME25505000012345678951",
                                regex: '^(ME)[0-9]{2}[0-9]{3}[0-9]{13}[0-9]{2}$'
                            },
                            /* country outside euro zone */
                            'RO': {
                                name: "Romania",
                                l: 24,
                                f: "U04A16",
                                t1: "y",
                                t2: "n",
                                euro: false,
                                eg: "RO49AAAA1B31007593840000",
                                regex: '^(RO)[0-9]{2}[A-Z]{4}[A-Z0-9]{16}$'
                            },
                            'SM': {
                                name: "San Marino",
                                l: 27,
                                f: "U01F05F05A12",
                                t1: "n",
                                t2: "n",
                                euro: true,
                                eg: "SM86U0322509800000000270100",
                                regex: '^(SM)[0-9]{2}[A-Z]{1}[0-9]{5}[0-9]{5}[A-Z0-9]{12}$'
                            },
                            /* country outside EU */
                            'BA': {
                                name: "Bosnia and Herzegovina",
                                l: 20,
                                f: "F03F03F08F02",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "BA391290079401028494",
                                regex: '^(BA)[0-9]{2}[0-9]{6}[0-9]{10}$'
                            },
                            'TR': {
                                name: "Turkey",
                                l: 26,
                                f: "F05A01A16",
                                t1: "n",
                                t2: "y",
                                euro: false,
                                eg: "TR330006100519786457841326",
                                regex: '^(TR)[0-9]{2}[0-9]{5}[A-Z0-9]{17}$'
                            },
                            'AL': {
                                name: "Albania",
                                l: 28,
                                f: "F08A16",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "AL47212110090000000235698741",
                                regex: '^(AL)[0-9]{2}[0-9]{8}[A-Z0-9]{16}$'
                            },
                            'TN': {
                                name: "Tunisia",
                                l: 24,
                                f: "F02F03F13F02",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "TN5914207207100707129648",
                                regex: '^(TN)[0-9]{2}[0-9]{5}[0-9]{15}$'
                            },
                            'AE': {
                                name: "United Arab Emirates",
                                l: 23,
                                f: "F03F16",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "AE070331234567890123456",
                                regex: '^(AE)[0-9]{2}[0-9]{3}[0-9]{16}$'
                            },
                            'BH': {
                                name: "Bahrein",
                                l: 22,
                                f: "U04A14",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "BH67BMAG00001299123456",
                                regex: '^(BH)[0-9]{2}[A-Z]{4}[0-9]{14}$'
                            },
                            'DO': {
                                name: "Dominican Republic",
                                l: 28,
                                f: "A04F20",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "DO28BAGR00000001212453611324",
                                regex: '^(DO)[0-9]{2}[A-Z0-9]{4}[0-9]{20}$'
                            },
                            'FO': {
                                name: "Faroe Islands",
                                l: 18,
                                f: "F04F09F01",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "FO6264600001631634",
                                regex: '^(FO)[0-9]{2}[0-9]{4}[0-9]{9}[0-9]{1}$'
                            },
                            'GE': {
                                name: "Georgia",
                                l: 22,
                                f: "U02F16",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "GE29NB0000000101904917",
                                regex: '^(GE)[0-9]{2}[A-Z]{2}[0-9]{16}$'
                            },
                            'GL': {
                                name: "Greenland",
                                l: 18,
                                f: "F04F09F01",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "GL8964710001000206",
                                regex: '^(GL)[0-9]{2}[0-9]{4}[0-9]{9}[0-9]{1}$'
                            },
                            'IL': {
                                name: "Israel",
                                l: 23,
                                f: "F03F03F13",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "IL620108000000099999999",
                                regex: '^(IL)[0-9]{2}[0-9]{3}[0-9]{3}[0-9]{13}$'
                            },
                            'KW': {
                                name: "Kuwait",
                                l: 30,
                                f: "U04A22",
                                t1: "n",
                                t2: "y",
                                euro: false,
                                eg: "KW81CBKU0000000000001234560101",
                                regex: '^(KW)[0-9]{2}[A-Z]{4}[0-9]{22}$'
                            },
                            'KZ': {
                                name: "Kazakhstan",
                                l: 20,
                                f: "F03A13",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "KZ86125KZT5004100100",
                                regex: '^(KZ)[0-9]{2}[0-9]{3}[A-Z0-9]{13}$'
                            },
                            'LB': {
                                name: "LEBANON",
                                l: 28,
                                f: "F04A20",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "LB62099900000001001901229114",
                                regex: '^(LB)[0-9]{2}[0-9]{4}[A-Z0-9]{20}$'
                            },
                            'MR': {
                                name: "Mauritania",
                                l: 27,
                                f: "F05F05F11F02",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "MR1300020001010000123456753",
                                regex: '^(MR)[0-9]{2}[0-9]{5}[0-9]{5}[0-9]{11}[0-9]{2}$'
                            },
                            'MU': {
                                name: "Mauritius",
                                l: 30,
                                f: "U04F02F02F12F03U03",
                                t1: "n",
                                t2: "n",
                                euro: false,
                                eg: "MU17BOMM0101101030300200000MUR",
                                regex: '^(MU)[0-9]{2}[A-Z]{4}[0-9]{2}[0-9]{2}[0-9]{12}[0-9]{3}$[A-Z]{3}'
                            },
                            'SA': {
                                name: "Saudi Arabia",
                                l: 24,
                                f: "F02A18",
                                t1: "n",
                                t2: "y",
                                euro: false,
                                eg: "SA0380000000608010167519",
                                regex: '^(SA)[0-9]{2}[0-9]{2}[A-Z0-9]{18}$'
                            },
                            'US': {
                                name: "United State",
                                l: 24,
                                f: "F02A18",
                                t1: "n",
                                t2: "y",
                                euro: false,
                                eg: "",
                                //                    regex:'^[0-9]{9} [0-9]{5,20}$'
                                regex: '^[0-9]{2,9} [0-9]{2,20}$'
                            },
                            'CA': {
                                name: "Canada",
                                l: 24,
                                f: "F02A18",
                                t1: "n",
                                t2: "y",
                                euro: false,
                                eg: "",
                                //                    regex:'^[0-9]{5,9} [0-9]{5,20}$'
                                regex: '^[0-9]{2,9} [0-9]{2,20}$'
                            }

                        },
                        filter: function (val) {
                            val = this.inherited(arguments);

                            if ((this.country != '') && (this.country != 'CA') && (this.country != 'US') && (typeof this.ibanexp[this.country] != 'undefined')) {
                                // ne supprimer les espaces que pour les comptes avec un n° IBAN
                                val = val.replace(/[ \\\/\-\.]/g, "");
                            }
                            return val;
                        },
                        validator: function (value, constraints) {
                            value = this.filter(value);
                            if (this._isEmpty(value)) {
                                return  (!this.required);
                            }
                            var country;
                            if (this.country == '') {
                                country = value.substr(0, 2);
                            } else {
                                country = this.country;
                            }


                            if (typeof this.ibanexp[country] == 'undefined') {
                                if (this.requiredIBANCountry) {
                                    this.invalidMessage = 'No IBAN code for this country';
                                    return false;
                                } else {
                                    this.invalidMessage = '';
                                    return true;
                                }
                            } else {

                                if (this.country == 'CA') {
                                    if (((new RegExp("^(?:" + this.ibanexp[country].regex + ")" + (this.required ? "" : "?") + "$")).test(value)) == false) {
                                        this.invalidMessage = 'Le format n\'est pas valide. Veuillez introduire le "Transit <espace> numéro de compte" (ex:12345001 1245512121221).<br />Invalid format. Please type the "Transit <blank> Account Number" (e.g.:12345001 1245512121221).';
                                        return false;
                                    } else {
                                        return true;
                                    }

                                }
                                if (this.country == 'US') {
                                    if (((new RegExp("^(?:" + this.ibanexp[country].regex + ")" + (this.required ? "" : "?") + "$")).test(value)) == false) {
                                        this.invalidMessage = 'Le format n\'est pas valide. Veuillez introduire le "ABA <espace> numéro de compte" (ex:123412349 1245512121221).<br />Invalid format. Please type the "ABA <blank> Account Number" (e.g.:123412349 1245512121221).';
                                        return false;
                                    } else {
                                        return true;
                                    }

                                }

                                if ((value.indexOf(' ') >= 0) || (value.indexOf('/') >= 0) || (value.indexOf('-') >= 0) || (value.indexOf('.') >= 0)) {
                                    this.invalidMessage = 'Le format n\'est pas valide. Veuillez introduire le code IBAN sans espace et sans séparateur.<br />Invalid format. Please type the IBAN code without blanks and separators.';
                                    return false;
                                }

                                if (((new RegExp("^(?:" + this.ibanexp[country].regex + ")" + (this.required ? "" : "?") + "$")).test(value)) == false) {
                                    this.invalidMessage = 'Format invalide. Le format attendu est: ' + this.ibanexp[country].eg + ' / Invalid format. Expected format is, e.g., ' + this.ibanexp[country].eg;
                                    return false;
                                }

                                if (!(this.IBANokay(value))) {
                                    this.invalidMessage = 'Le numéro IBAN n\'est pas correct: les chiffres de contrôle ne sont pas valides / Incorrect IBAN. Error in control digits';
                                    return false;
                                }
                                return true;
                            }
                        },
                        fz: function (p, l) {
                            while (p.length < l) {
                                p = "0" + p;
                            }
                            return p;
                        },
                        IBANokay: function (iban) {
                            return this.cksum(iban) == "97";
                        },
                        cksum: function (iban) {
                            var cd = iban.substring(0, 2);
                            var ck = iban.substring(2, 4);
                            var bban = iban.substring(4);
                            var d = "";
                            for (var i = 0; i < bban.length; ++i) {
                                var ch = bban.charAt(i);
                                if ("0" <= ch && ch <= "9") {
                                    d += ch;
                                } else {
                                    d += this.c2d(ch);
                                }
                            }
                            for (var i = 0; i < cd.length; ++i) {
                                var ch = cd.charAt(i);
                                d += this.c2d(ch);
                            }
                            d += ck;
                            ck = 98 - this.m97(d);
                            return this.fz("" + ck, 2);
                        },
                        c2d: function (ch) {
                            var upp = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            for (var i = 0; i < upp.length; ++i) {
                                if (ch == upp.charAt(i)) {
                                    break;
                                }
                            }
                            return i + 10;
                        },
                        m97: function m97(digs) {
                            var m = 0;
                            for (var i = 0; i < digs.length; ++i) {
                                m = (m * 10 + parseInt(digs.charAt(i))) % 97;
                            }
                            return m;
                        }

                    });

            return amster.form.IbanTextBox;

        });
