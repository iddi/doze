define(
        ["dojo",
            "dijit",
            "dojo/_base/declare",
            "dijit/form/ValidationTextBox",
            "dojo/i18n!./nls/message"],
        function (doze, dijit, declare, i18n) {

            //TODO utiliser  le MaskedEditTextBox: masque:99.99.99-999.99

            return declare("amster.form.BelgianNationalRegisterTextBox", [dijit.form.ValidationTextBox],
                    {
                        // summary:
                        //		summary
                        //
                        // description:
                        //		description

                        regExp: '[0-9]{11}',
                        nationality: 'BE',
                        birthdate: '',
                        sex: '',
                        required: false,
                        requiredforbelgian: true,
                        trimNumber: function (s) {
                            while (s.substr(0, 1) == '0' && s.length > 1) {
                                s = s.substr(1, 9999);
                            }
                            return s;
                        },
                        filter: function (val) {
                            val = this.inherited(arguments);
                            if ((typeof val) == 'string') {
                                val = val.replace(/[ \\\/\-\.]/g, "");
                            }
                            return val;
                        },
                        validator: function (value, constraints) {
                            value = this.filter(value);

                            if (this._isEmpty(value) && (!this.required) && (!this.requiredforbelgian || (this.nationality != 'BE'))) {
                                return true;
                            }
                            if ((value.indexOf(' ') >= 0) || (value.indexOf('/') >= 0) || (value.indexOf('-') >= 0) || (value.indexOf('.') >= 0)) {
                                this.invalidMessage = i18n.INVALID_CHAR;
                                return false;
                            }

                            if (((new RegExp("^(?:" + this.regExpGen(constraints) + ")" + (this.required ? "" : "?") + "$")).test(value)) == false) {
                                this.invalidMessage = 'Format invalide. Le format attendu est: nnnnnnnnnnn / Invalid format. Expected format : nnnnnnnnnnn';
                                return false;
                            }


                            /* check sex H-> odd; F -> even*/
                            //       if (sex != '') {
                            //           if (sex == 'H' && (!(parseInt(value.substring(6,9).replace(/^0*/g, ""))%2))) { return false; }
                            //           if (sex == 'F' && (parseInt(value.substring(6,9).replace(/^0*/g, ""))%2)) { return false; }
                            //       }

                            /* check birthdate */
                            if (this.birthdate) {

                                var checked = false;
                                var m = dojo.date.locale.format(this.birthdate, {
                                    datePattern: "MM",
                                    selector: "date"
                                });
                                var y = dojo.date.locale.format(this.birthdate, {
                                    datePattern: "yy",
                                    selector: "date"
                                });
                                var d = dojo.date.locale.format(this.birthdate, {
                                    datePattern: "dd",
                                    selector: "date"
                                });
                                var s = y.toString() + m.toString() + d.toString();
                                if (s == value.substring(0, 6)) {
                                    checked = true;
                                }

                                // allow null date
                                s = y.toString() + '0000';
                                if (s == value.substring(0, 6)) {
                                    checked = true;
                                }

                                if (this.nationality != 'BE') {
                                    // for the non-belgian allow the temporary belgian register number
                                    m = parseInt(this.trimNumber(m)) + 20;
                                    s = y.toString() + m.toString() + d.toString();
                                    //if(window.console) console.log(s);
                                    if (s == value.substring(0, 6)) {
                                        checked = true;
                                    }

                                    m = parseInt(m) + 20;
                                    s = y.toString() + m.toString() + d.toString();
                                    //if(window.console) console.log(s);
                                    if (s == value.substring(0, 6)) {
                                        checked = true;
                                    }

                                }
                                if (!checked) {
                                    this.invalidMessage = 'Incohérence entre votre date de naissance et votre numéro national / Incoherence between your birthdate and your national register number.';
                                    return false;
                                }
                            }

                            /* check sum */

                            if ((parseInt(value.substring(9, 11).replace(/^0*/g, "")) != (97 - (parseInt(value.substring(0, 9).replace(/^0*/g, "")) % 97)))) {
                                this.invalidMessage = 'Le numéro national n\'est pas correct: les chiffres de contrôle ne sont pas corrects / Incorrect National Number. Error in control digits';
                                return false;
                            }

                            return true;
                            /*
                             } else {
                             if (((new RegExp("^(?:" + this.regExpGen(constraints) + ")"+(this.required?"":"?")+"$")).test(value)) == false) {
                             this.invalidMessage = 'Invalid format. Expected format : nnnnnnnnnnn';
                             return false;
                             }
                             return true;
                             }*/
                        }

                    });
            return amster.form.BelgianNationalRegisterTextBox;

        });
