define({
    root:
            ({
                INVALID_CHAR: 'Le format n\'est pas valide. Veuillez introduire le numéro sans espace et sans séparateur.<br />Invalid format. Please type the numéro without blanks and separators.',
                INVALID_FORMAT: 'Format invalide. Le format attendu est: ',
                INVALID_CHECKSUM: 'La valeur n\'est pas correcte: les chiffres de contrôle ne sont pas valides / Incorrect value. Error in control digits'
            }),
    "de": 1,
    "en": 1,
    "nl": 1,
    "fr": 1
});
