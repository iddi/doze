define(
        ({
            INVALID_FORMAT: 'Format invalide. Le format attendu est: ',
            INVALID_CHECKSUM: 'La valeur n\'est pas correcte: les chiffres de contrôle ne sont pas valides',
            INVALID_CHAR: 'Le format n\'est pas valide. Veuillez introduire la valeur sans espace et sans séparateur.'
        })
        );