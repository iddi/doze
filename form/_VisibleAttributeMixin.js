define([
  "dojo/_base/declare",
  "dojo/dom-style"
], function(declare, domStyle) {
	/*
      * This mixin makes it possible to set the "display" style property of 
      * the DOM node (of any widget) as a Widget property and thus bind it to an MVC model
      * when needed.
      */
    return  declare("doze.form._VisibleAttributeMixin", [], {
         // parameters
         visible: true,

         _setVisibleAttr: function(/*String*/ visible){
             this._set("visible", visible);
             domStyle.set(this.domNode, "display", visible?"inline-block":"none");
         }
     });
});
