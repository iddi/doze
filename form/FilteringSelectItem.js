﻿define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dijit/form/FilteringSelect"
], function (declare,
        lang,
        FilteringSelect
        ) {
    return declare("doze.form.FilteringSelectItem", [FilteringSelect],
            {
                _setValueAttr: function (value) {
                    if (typeof value[this.store.idProperty] !== 'undefined') {
                        return this.inherited(arguments,
                                [value[this.store.idProperty]]);
                    } else {
                        return this.inherited(arguments);
                    }
                },
                _getValueAttr: function () {
                    return this.get('item');
                }
            });
});