define(
        ["dojo/_base/declare",
            "dijit/form/DateTextBox"],
        function (declare, DateTextBox) {


            return declare("doze.form.DateWithoutPopupTextBox", [DateTextBox],
                    {
                        popupClass: "",
                        hasDownArrow: false,
                        openDropDown: function () {
                        },
                        //"_stopClickEvents": "::false",
                        _onDropDownMouseUp: function () {
                        },
                        _onDropDownMouseDown: function () {
                        }

                    });

        });
