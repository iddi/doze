define(
        ["dojo", "dijit", "dijit/form/ValidationTextBox"],
        function (dojo, dijit) {

            //TODO utiliser  le MaskedEditTextBox: masque:AA99 **** **** **** **** **** **** **
            //TODO add a list_of_accepted_countries + possibility EUROZONE, EU, EUROPE, ALL /* default */



            dojo.declare("doze.form.BicTextBox", [dijit.form.ValidationTextBox],
                    {
                        // summary:
                        //		summary
                        //
                        // description:
                        //		description

                        regExp: '([A-Z]{4}[A-Z]{2}[A-Z0-9]{2}([A-Z0-9]{3})?)',
                        uppercase: true,
                        trim: true,
                        country: '',
                        filter: function (val) {
                            val = this.inherited(arguments);
                            val = val.replace(/[ \\\/\-\.]/g, "");
                            return val;
                        },
                        validator: function (value, constraints) {
                            value = this.filter(value);

                            if (this._isEmpty(value)) {
                                return  (!this.required);
                            }
                            if (this.country == '') {
                                return true;
                            }

                            if (value.length < 8) {
                                //this.invalidMessage = 'Le format n\'est pas valide. Veuillez introduire le code BIC correcte sans espace (ex:INGBBE2A).<br />Invalid format. Please type the BIC code without blanks (e.g.;INGBBE2A).';
                                this.invalidMessage = 'Le format n\'est pas valide. Veuillez introduire le code BIC correcte sans espace (ex: xxxx' + this.country + 'xx).<br />Invalid format. Please type the BIC code without blanks (e.g.: xxxx' + this.country + 'xx).';
                                return false;
                            }

                            if (value.indexOf(' ') >= 0) {
                                this.invalidMessage = 'Le format n\'est pas valide. Veuillez introduire le code BIC correcte sans espace (ex: xxxx' + this.country + 'xx).<br />Invalid format. Please type the BIC code without blanks (e.g.;xxxx' + this.country + 'xx).';
                                return false;
                            }

                            var country = value.substr(4, 2);
                            if (country != this.country) {
                                this.invalidMessage = 'Vérifier que le pays de la banque sélectionné correspond à votre banque.<br />Make sure the selected country for the banking agency matches your bank.';
                                return false;
                            }



                            if (((new RegExp("^(?:" + '^[A-Z]{4}[A-Z]{2}[A-Z0-9]{2}([A-Z0-9]{3})?$' + ")" + (this.required ? "" : "?") + "$")).test(value)) == false) {
                                this.invalidMessage = 'Le numéro de BIC introduit n\'est pas valide. (ex: xxxx' + this.country + 'xx).<br />Invalid BIC. (e.g.: xxxx' + this.country + 'xx).';
                                return false;
                            }

                            return true;
                        }

                    });
            return amster.form.BicTextBox;

        });
  