define(
        [
            "dojo/_base/declare",
            "dojo/query",
            "dojo/on",
            "dojo/text!doze/form/resources/TransferBox.html",
            "dojo/dom-style",
            "dojo/store/JsonRest",
            "dijit/form/_FormValueWidget",
            "dijit/_WidgetsInTemplateMixin",
            "dijit/form/Button",
            "dijit/Tooltip",
            "dojo/mouse",
            "dgrid/OnDemandList",
            "dgrid/Selection",
            "dgrid/Keyboard",
            "dojo/dom-construct",
            "dojo/store/Observable",
            "dojo/store/Memory",
            "dojo/json",
            "dojo/_base/lang"
        ], function (declare,
        query,
        on,
        TransferBoxTemplate,
        domStyle,
        JsonRest,
        _FormValueWidget,
        WidgetsInTemplateMixin,
        Button,
        Tooltip,
        mouse,
        List,
        Selection,
        Keyboard,
        domConstruct,
        Observable,
        Memory,
        JSON,
        lang
        ) {

    var TBList = declare([List, Selection, Keyboard]);


    return declare("doze.form.TransferBox", [_FormValueWidget, WidgetsInTemplateMixin], {
        templateString: TransferBoxTemplate,
        baseClass: "dozeTransferBox",
        labelField: "name",
        valueField: "id",
        // disabled: boolean
        //		Whether or not this widget is disabled
        disabled: false,
        // readOnly: boolean
        //		Whether or not this widget is readOnly
        readOnly: false,
        // required: Boolean
        //		User is required to check at least one item.
        required: false,
        // invalidMessage: String
        //		The message to display if value is invalid.
        invalidMessage: "You must select an item", // "$_unset_$", // TODO nls

        // _message: String
        //		Currently displayed message
        _message: "",
        //_startStyle: '',
        //_btnNode: null,

        sortProperty: "id", //TO Modify
        selectionMode: "extended",
        selectionToDisable: function (list, button) {
            var selected = 0;
            var _this = this;
            list.on("dgrid-select", function (e) {
                selected += e.rows.length;
                if (!_this.readOnly && !_this.disabled) {
                    button.set("disabled", !selected);
                }
            });
            list.on("dgrid-deselect", function (e) {
                selected -= e.rows.length;
                if (!_this.readOnly && !_this.disabled) {
                    button.set("disabled", !selected);
                }
            });
        },
        postCreate: function () {
            this.addButton.set("disabled", true);
            this.removeButton.set("disabled", true);
            this.memory_store = new Memory(); // sinon problème avec les getvalue et setvalue voir dans les listes déroumlantes comment c'est fait pour résoudre le déffered au démarrage

            var _this = this;
            console.log('construct');
            this.store.query('?name=*').then(function (results) {
                console.log('promise');
                //console.log(results);

                var data = new Array();
                for (var key in results) {
                    data.push({'id': results[key][_this.valueField],
                        'name': results[key][_this.labelField]});
                }

                _this.memory_store = Observable(new Memory({
                    identifier: "id",
                    data: data
                }));
                _this.from = new TBList({
                    store: _this.memory_store,
                    selectionMode: _this.selectionMode,
                    query: function (item) {
                        return !item.__selected;
                    },
                    sort: _this.sortProperty,
                    renderRow: _this.renderItem
                }, _this.fromNode);
                _this.selectionToDisable(_this.from, _this.addButton);

                _this.to = new TBList({
                    store: _this.memory_store,
                    selectionMode: _this.selectionMode,
                    query: function (item) {
                        return item.__selected;
                    },
                    sort: _this.sortProperty,
                    renderRow: _this.renderItem
                }, _this.toNode);
                _this.selectionToDisable(_this.to, _this.removeButton);


            });
            this.inherited(arguments);
        },
        _setValueAttr: function (value) {
            if (!value) {
                value = [];
            } else {
                if (typeof value == "string") {
                    value = JSON.parse(value, true);
                }
                if (!lang.isArray(value)) {
                    value = [value];
                }
            }

            // clear all selected items
            var _this = this;
            var items = this.memory_store.query(function (item) {
                return item.__selected;
            }).forEach(function (item) {
                item.__selected = false;
                _this.memory_store.put(item);
            });

            console.log(value);
            //var notify = !this.from || !this.from._started;
            //console.log(notify);
            for (var i = 0; i < value.length; i++) {
                var item = this.memory_store.get(value[i]);
                if (!item) {
                    console.log("item not found for value:" + value[i])
                    continue;
                }
                item.__selected = true;
                console.log(item);
//                notify && this.memory_store.put(item);
                this.memory_store.put(item);
            }
            this.inherited(arguments);

        },
        _getValueAttr: function () {
            console.log('get value');
            var store = this.memory_store;
            return store.query(function (item) {
                return item.__selected;
            }).map(function (item) {
                return store.getIdentity(item);
            });
        },
        add: function () {
            console.log('add');
            this.addButton.set("disabled", true);
            for (var id in this.from.selection) {
                var row = this.from.row(id);
                row.data.__selected = true;
                this.memory_store.put(row.data);
            }
            this.set('value', this._getValueAttr());

        },
        remove: function () {
            console.log('remove');
            this.removeButton.set("disabled", true);
            for (var id in this.to.selection) {
                var row = this.to.row(id);
                row.data.__selected = false;
                this.memory_store.put(row.data);
            }
            this.set('value', this._getValueAttr());

        },
        renderItem: function (item) {
            return domConstruct.create("div", {
                innerHTML: item.name
            });
        },
        validate: function (isFocused) {
            //TODO add required 'min 1 item selected 
            // TODO add min selected item 
            // TODO add max selected item 
            Tooltip.hide(this.domNode);
            var isValid = this.isValid(isFocused);
            if (!isValid) {
                this.displayMessage(this.invalidMessage);
            }
            return isValid;
        },
        isValid: function (/*Boolean*/ isFocused) {
            // summary:
            //		Tests if the required items are selected.
            //		Can override with your own routine in a subclass.
            // tags:
            //		protected
            return this.validator();
        },
        getErrorMessage: function (/*Boolean*/ isFocused) {
            // summary:
            //		Return an error message to show if appropriate
            // tags:
            //		protected
            return this.invalidMessage;
        },
        displayMessage: function (/*String*/ message) {
            // summary:
            //		Overridable method to display validation errors/hints.
            //		By default uses a tooltip.
            // tags:
            //		extension
            Tooltip.hide(this.domNode);
            if (message) {
                Tooltip.show(message, this.domNode, this.tooltipPosition);
            }
        },
        _refreshState: function () {
            // summary:
            //		Validate if selection changes.
            this.validate(this.focused);
        },
        onChange: function (newValue) {
            // summary:
            //		Validate if selection changes.
            this._refreshState();
        },
        reset: function () {
            // Overridden so that the state will be cleared.
            this.inherited(arguments);
            Tooltip.hide(this.domNode);
        },
        _setDisabledAttr: function (value) {
            // summary:
            //		Disables (or enables) all the children as well
            this.disabled = value;
            this.removeButton.set("disabled", true);
            this.addButton.set("disabled", true);
        },
        _setReadOnlyAttr: function (value) {
            // summary:
            //		Sets read only (or unsets) all the children as well
            if (value) {
                domStyle.set(this.removeButton.domNode, 'display', 'none');
                domStyle.set(this.addButton.domNode, 'display', 'none');
                domStyle.set(this.fromNode, 'display', 'none');
            } else {
                domStyle.set(this.removeButton.domNode, 'display', 'inline-block');
                domStyle.set(this.addButton.domNode, 'display', 'inline-block');
                domStyle.set(this.fromNode, 'display', 'inline-block');

            }

            this.readOnly = value;
        }
    });
});


