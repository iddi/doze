define(
        ["dojo", "dijit", "dijit/form/ValidationTextBox"],
        function (dojo, dijit) {

            //TODO utiliser  le MaskedEditTextBox: masque:AA99 **** **** **** **** **** **** **
            //TODO add a list_of_accepted_countries + possibility EUROZONE, EU, EUROPE, ALL

            dojo.declare("doze.form.BelgianIdCardNumberTextBox", [dijit.form.ValidationTextBox],
                    {
                        regExp: '[0-9]{10}[0-9]{2}',
                        expected: '451776954273',
                        uppercase: false,
                        trim: true,
                        validator: function (value, constraints) {
                            if (this._isEmpty(value)) {
                                return  (!this.required);
                            }
                            value = value.toUpperCase();

                            if ((value.indexOf('/') >= 0) || (value.indexOf('-') >= 0) || (value.indexOf('.') >= 0)) {
                                this.invalidMessage = 'Le format n\'est pas valide. Veuillez introduire la valeur sans espace et sans séparateur.<br />Invalid format. Please type the value without blanks and separators.';
                                return false;
                            }

                            if (((new RegExp("^(?:" + this.regExp + ")" + (this.required ? "" : "?") + "$")).test(value)) == false) {
                                this.invalidMessage = 'Format invalide. Le format attendu est: ' + this.expected + ' / Invalid format. Expected format is, e.g., ' + this.expected;
                                return false;
                            }

                            if (!(this.isMod97(value))) {
                                this.invalidMessage = 'La valeur n\'est pas correcte: les chiffres de contrôle ne sont pas valides / Incorrect value. Error in control digits';
                                return false;
                            }
                            return true;

                        },
                        isMod97: function (val) {
                            var entire = val.substring(0, (val.length) - 2);
                            var rest = val.substring((val.length) - 2, val.length);

                            var mod = entire % 97;

                            if ((mod == rest && rest != 0) || (mod == 0 && rest == 97))
                                return true
                            else
                                return false;
                        }
                    });
            return amster.form.BelgianIdCardNumberTextBox;
        });