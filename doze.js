define([
    "./definition",
    "./store",
    "./template",
    "./mvc/Generate",
    "./router/catchall",
    "./router/module",
    "./widget/view",
    "./widget/edit",
    "./widget/filter",
    "./widget/grid",
    "./widget/search",
    "./widget/tabs"
], function (amster) {
    //>>pure-amd
    // module:
    //		doze
    // summary:
    //		The main doze object
    return amster;
});
