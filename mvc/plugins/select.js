define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/topic",
    "dijit/registry",
    "dojo/on",
    "dojo/_base/array",
    "dojo/query",
    "dijit/Menu",
    "dijit/MenuItem",
    "dojo/query!css2"
], function (declare, lang, topic, registry, on, array, query, Menu, MenuItem) {
    return declare("doze.mvc.plugins.select", null,
            {
                constructor: function (options) {
                    // summary:
                    //		This is a basic store for RESTful communicating with a server through JSON
                    //		formatted data.
                    // options: dojo/store/JsonRest
                    //		This provides any configuration information that will be mixed into the store
                    declare.safeMixin(this, options);
                },
                dozeTarget: '',
                currentWidgetId: '',
                onNew: function () {
                    return;
                },
                onSearch: function () {
                    return;
                },
                onEdit: function (itemId) {
                    return;
                },
                register: function () {
                    var _this = this;
                    var menu = new Menu({
                        targetNodeIds: ["applicationLayout"],
                        selector: "[data-doze-target=" + this.dozeTarget + "] label",
                        leftClickToOpen: true
                    });
                    on(menu, "open", function (evt) {
                        var row = this.currentTarget.parentNode;
                        var w = registry.getEnclosingWidget(query('input', row)[0]);
                        var itemId = parseInt(w.get('value')) || false;
                        _this.currentWidgetId = w.id;
                        array.forEach(this.getChildren(), function (child) {
                            child.set('disabled', (itemId === false && child.needId === true));
                            _this.itemId = itemId;
                        });
                    });
                    menu.addChild(new MenuItem({
                        label: "Edit",
                        needId: true,
                        itemId: null,
                        onClick: lang.hitch(_this, "onEdit")
                    }));
                    menu.addChild(new MenuItem({
                        label: "New",
                        needId: false,
                        onClick: lang.hitch(_this, "onNew")
                    }));
                    menu.addChild(new MenuItem({
                        label: "Search",
                        needId: false,
                        onClick: lang.hitch(_this, "onSearch")
                    }));

                    topic.subscribe(this.dozeTarget, function (item) {
                        var _sel = registry.byId(_this.currentWidgetId);
                        _sel.set('item', item);
                    });
                }
            });
});