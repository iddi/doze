//>>built
define(
        ({
            APPLY_FILTER: 'Chercher',
            ADD_FILTER: 'Ajouter un filtre',
            ADD_CRITERIA: 'Ajouter un critère...',
            PRESELECT_FILTER: 'Préselection',
            REMOVE_FILTER: 'Supprimer',
            CLICKONGLASS: 'Utiliser la loupe pour choisir une valeur',
			CLICK_ON_OBTAIN_RESULTS: 'Cliquer sur "Chercher" pour obtenir les résultats',
            CUSTOMFILTER: 'Filtres personnalisés',
            CUSTOMFILTERMENUEDIT: 'Edit'
        })
        );