define(
        ({
            APPLY_FILTER: 'Search',
            ADD_FILTER: 'Add filter',
            REMOVE_FILTER: 'Delete',
            ADD_CRITERIA: 'Add criteria on ...',
            CLICKONGLASS: 'Click',
			CLICK_ON_OBTAIN_RESULTS: 'Click on search to obtain results',
            CUSTOMFILTER: 'Custom filters',
            CUSTOMFILTERMENUEDIT: 'Edit'
        })



        );