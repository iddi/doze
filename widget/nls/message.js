define({
    root:
            ({
                APPLY_FILTER: 'Search',
                ADD_FILTER: 'Add filter',
                ADD_CRITERIA: 'Add criteria on ...',
                REMOVE_FILTER: 'Delete',
                PRESELECT_FILTER: 'Preselect',
                CLICKONGLASS: 'Click on magnifying glass to select a value',
				CLICK_ON_OBTAIN_RESULTS: 'Click on search to obtain results',
                CUSTOMFILTER: 'Custom filters',
                CUSTOMFILTERMENUEDIT: 'Edit'
            }),
    "de": 1,
    "en": 1,
    "nl": 1,
    "fr": 1
});
