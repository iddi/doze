define(
        [
            "dojo/_base/declare",
            "dojo/on",
            "dojo/_base/lang",
            "dojo/_base/array",
            "dojo/_base/json",
            "dojo/has",
            "dojo/topic",
            "dojo/window",
            "dojo/Deferred",
            "dojo/Stateful",
            "dijit/registry",
            "dojox/mvc/EditStoreRefController",
            "doze/router/module",
            "dojox/mvc/equals",
            "dojox/mvc/Output",
            "dojox/mvc/getStateful"

        ],
        function (
                declare,
                on,
                lang,
                array, json, has, topic, winUtils, Deferred, Stateful,
                registry,
                EditStoreRefController, router, equals, Output, getStateful
                ) {


            return declare("doze.widget._mvcMixin", [], {
                storeDefinition: null,
                schemaDefinition: null,
                controller: null,
                editionMode: false,
                insertMode: true,
                dirty: false,
                labelProperty: '',
                defaultValues: null,
                noRedirect: false,
                publishOn: false,
                postMixInProperties: function () {
                    this.inherited(arguments);
                    this.controller = new EditStoreRefController({store: this.storeDefinition.getStore()});
                    var _this = this;
                    this.controller.watch(function (prop, oldValue, newValue) {
                        var dirty = _this.get('dirty');
                        var dirtyState = !equals(_this.controller[_this.controller._refOriginalModelProp], _this.controller[_this.controller._refEditModelProp]);
                        if (dirtyState !== dirty) {
                            _this.set('dirty', dirtyState);
                        }
                        if (_this.controller && _this.controller.store && _this.controller.idProperty && prop === _this.controller.store.idProperty) {
                            _this.set('editionMode', (typeof newValue !== 'undefined'));
                            _this.set('insertMode', (typeof newValue === 'undefined'));
                        }
                        if (_this.labelProperty && prop === _this.labelProperty) {
                            _this.set('label', newValue);
                        }
                    });
                },
                _getDiffAttr: function () {
                    var from = this.controller[this.controller._refOriginalModelProp];
                    var to = this.controller[this.controller._refEditModelProp];
                    var diff = {};
                    for (var p in from) {
                        if (typeof from[p] !== 'function' && !equals(from[p], to[p])) {
                            diff[p] = {from: from[p], to: to[p]};
                        }
                    }
                    for (var p in to) {
                        if (!diff[p] && typeof to[p] !== 'function' && !equals(from[p], to[p])) {
                            diff[p] = {from: from[p], to: to[p]};
                        }
                    }
                    return diff;
                },
                _setDirtyAttr: function (value) {
                    this.inherited(arguments);
                    this._set('dirty', value);
                },
                getDefaultItem: function () {
                    var defaultValuesFromSchema = this.schemaDefinition.getDefault();
                    return lang.mixin(defaultValuesFromSchema, this.defaultValues || {});
                },
                activate: function (args) {
                    var args = args || {};
                    this.noRedirect = args.noRedirect || false;
                    this.publishOn = args.publishOn || false;
                    var d = new Deferred();
                    var _this = this;
                    if (equals(args, {}) || args.insert === true || args.insert === 'true') {
                        try {
                            _this.currentId = null;
                            var def = getStateful(lang.mixin(this.getDefaultItem(), args), this.controller.getStatefulOptions);
                            this.updateSourceModel(def);
                            d.resolve(def);
                        } catch (err) {
                            this._emitError(err);
                            d.reject(e);
                        }
                    }
                    if (args.id) {
                        if (_this.currentId === args.id) {
                            d.resolve(_this.controller[_this.controller._refEditModelProp]);
                        } else {
                            this.controller.getStore(args.id).then(function (item) {
                                if (item === null) {
                                    this.emitError('item does not exists');
                                    d.reject('item does not exists');
                                }
                                _this.set('currentId', args.id);
                                d.resolve(item);
                            },
                                    function (err) {
                                        _this._emitError(err);
                                        d.reject(err);
                                    }
                            );
                        }
                    } else {
                        d.resolve({});
                    }

                    return d.promise;
                },
                updateSourceModel: function (def) {
                    this.controller.set(this.controller._refSourceModelProp, def);
                },
                _changeRoute: function (item) {
                    var _this = this;
                    router.change(_this, {id: _this.controller.store.getIdentity(item)}, true, {insert: null});
                },
                save: function () {
                    var d = new Deferred();
                    if (this.validate() !== true) {
                        this._emitError('Some controls are in invalid state.');
                        d.reject('Some controls are in invalid state.');
                        return d.promise;
                    }
                    var obj = this.controller.model;
                    var id = this.controller.store.getIdentity(obj);
                    var _this = this;

                    if (typeof id === 'undefined') {
                        var _this = this;
                        this.controller.addStore(this.controller.model).then(
                                function (item) {
                                    try {
                                        _this.updateSourceModel(new Stateful(item));
                                        if (_this.noRedirect === false) {
                                            _this._changeRoute(item);
                                        }
                                        if (_this.publishOn) {
                                            d.promise.then(function (item) {
                                                topic.publish(_this.publishOn, item);
                                                _this.closeParent();
                                                return item;
                                            });
                                        }
                                        d.resolve(item);
                                    } catch (err) {
                                        _this._emitError(err);
                                        d.reject(err);
                                    }
                                },
                                function (err) {
                                    _this._emitError(err);
                                    d.reject(err);
                                }
                        );
                    } else {
                        var _this = this;
                        this.controller.putStore(this.controller.model).then(
                                function (item) {
                                    try {
                                        _this.updateSourceModel(new Stateful(item));
                                        d.resolve(item);
                                        _this.closeParent();
                                    } catch (err) {
                                        _this._emitError(err);
                                        d.reject(err);
                                    }
                                },
                                function (err) {
                                    _this._emitError(err);
                                    d.reject(err);
                                }
                        );
                    }
                    //return d.promise.then(); //??
                    return d.promise;
                },
                // TODO: corriger cela dans le version DOZE 2.0
                // même fonctionnalité que cancel mais le cancel est surchargé par le contentPane
                cancelModification: function () {
                    var d = new Deferred();
                    try {
                        this.controller.reset();
                        d.resolve(true);
                    } catch (err) {
                        this._emitError(err);
                        d.reject(err);
                    }
                    return d.promise;

                },
                cancel: function () {
                    var d = new Deferred();
                    try {
                        this.controller.reset();
                        d.resolve(true);
                    } catch (err) {
                        this._emitError(err);
                        d.reject(err);
                    }
                    return d.promise;

                },
                remove: function () {
                    var obj = this.controller.model;
                    var id = this.controller.store.getIdentity(obj);
                    var _this = this;
                    var d = new Deferred();
                    if (typeof id !== 'undefined') {
                        return this.controller.removeStore(id).then(
                                function (deleted) {
                                    if (deleted === true || json.fromJson(deleted) === true) {
                                        //router.change(_this, {insert: true}, true, {id: null});
                                        //_this.activate();
                                        _this.closeParent();
                                    }
                                    d.resolve(deleted);
                                },
                                function (err) {
                                    _this._emitError(err);
                                    d.reject(err);
                                }
                        );
                    }
                    return d.promise;
                },
                getWidgetsToValidate: function () {
                    return [];
                },
                validate: function () {
                    var didFocus = false;
                    var _this = this;
                    var isValid = array.every(_this.getWidgetsToValidate(), function (idWidget) {
                        var widget = registry.byId(idWidget);

                        if (!widget || widget.get('readOnly') === true || !widget.validate || typeof widget.validate !== 'function')
                            return true;
                        var valid = widget.validate();
                        if (!valid && !didFocus) {
                            winUtils.scrollIntoView(widget.containerNode || widget.domNode);
                            widget.focus();
                            didFocus = true;
                        }
                        return valid;
                    });

                    if (isValid) {
                        isValid = _this.validateWithSchema();
                    }
                    return isValid;
                },
                validateWithSchema: function () {
                    return true;
//                    if (this.schema === null)
//                        return true;
//                    if (this.schema.validators) {
//                        var ers = '';
//                        var isValid = true;
//                        for (var key in this.schema.validators) {
//                            var validator = lang.getObject(key, false, this.schema.validators);
//                            if (validator.validate && lang.isFunction(validator.validate)) {
//                                var valid = validator.validate(this.ctrl.model);
//                                if (!valid) {
//                                    isValid = false;
//                                    ers += validator.errorMessage + '\n';
//                                }
//                            }
//                        }
//                        this.set('errors', (isValid) ? '' : ers);
//                    }
//                    return isValid;
                }

            });
        });