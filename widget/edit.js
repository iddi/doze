define(
        [
            "dojo/_base/declare",
            "dojo/_base/lang",
            "dojo/text!./templates/edit.html",
            "doze/mvc/Generate",
            "./view",
            "./_mvcMixin",
            "dijit/Tooltip",
            "dojo/on",
            "dojo/mouse"
        ],
        function (declare,
                lang,
                template,
                Generate, View, _mvcMixin,
                Tooltip,
                on,
                mouse) {
            return declare("doze.widget.edit",
                    [View, _mvcMixin],
                    {
                        templateString: template,
                        generator: null,
                        getWidgetsToValidate: function () {
                            return this.generator.widgets;
                        },
                        _setDirtyAttr: function (value) {
                            this.inherited(arguments);
                            this.generator.set('dirty', value);
                        },
                        _initializeInterfaces: function () {
                        },
                        postCreate: function () {
                            this.generator.set('datas', this.controller);
                            this.generator.set('children', this.schemaDefinition.getSchema());
                            this.generator.set('displayGroupsTemplate', this.displayGroupsTemplate);
                            this._initializeInterfaces();
                            this.generator.startup();
                            this.inherited(arguments);
                        },
                        _getOriginalModelAttr: function () {
                            return this.controller.get(this.controller._refOriginalModelProp) || {};
                        },
                        _getModelAttr: function () {
                            return this.controller.get(this.controller._refEditModelProp) || {};
                        },
                        desactivate: function () {
                            var dirt = this.get('dirty');
                            if (dirt === true && this.saveButton) {
                                var node = this.saveButton.domNode;
                                var message = "<table><tr><td><span class='tooltipIcon icon-dozeexclamation-sign'>&nbsp;</span></td><td  class='tooltipMessage'>Before leaving this page.<br/>Save or cancel modifications.</td></tr></table>"
                                Tooltip.show(message, node);
                                on.once(node.parentNode, mouse.enter, function () {
                                    Tooltip.hide(node);
                                });
                            }
                            return !!!dirt;
                        }

                    }
            );
        });