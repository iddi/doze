define(
        [
            "dojox/mvc/at",
            "dojox/mvc/EditStoreRefController",
            "doze/router/module",
            "dojox/mvc/equals",
            "dojox/mvc/Output",
            "dojox/mvc/getStateful"
        ],
        function (
                at, EditStoreRefController, router, equals, Output, getStateful
                ) {
            dojo.global.at = at;
            return at;
        });