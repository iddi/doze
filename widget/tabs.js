define(
        [
            "dojo/_base/declare",
            "doze/widget/view",
            "dijit/registry",
            "dojo/_base/array",
            "dojo/aspect",
            "dijit/layout/StackContainer",
            "dijit/layout/ContentPane",
            "dijit/layout/TabContainer"
        ],
        function (declare, view, registry, array, aspect) {

            return declare("doze.widget.tabs",
                    [view],
                    {
                        tabsIdMapping: null,
                        tabsNameMapping: null,
                        tabToAllwaysActivate: null,
                        postCreate: function () {
                            this.inherited(arguments);
                            var _this = this;
                            _this.tabsIdMapping = {};
                            _this.tabsNameMapping = {};
                            array.forEach(_this.get('_attachPoints'), function (name) {
                                if (_this.stackView.getIndexOfChild(_this[name]) > -1) {
                                    _this.tabsIdMapping[_this[name].id] = name;
                                    _this.tabsNameMapping[name] = _this[name].id;
                                }
                            });
                            if (_this.stackView.tablist) {
                                _this.stackView.tablist.onButtonClick = function (page) {
                                    _this.routeArgs.tab = _this.tabsIdMapping[page.id];
                                    _this.routeArgs.md5 = _this.md5;
                                    _this.redirect(null, null, null, _this.routeArgs);
                                };
                                aspect.after(_this.stackView.tablist._menuBtn, 'loadDropDown', function (args) {
                                    array.forEach(_this.stackView.tablist._menuBtn.dropDown.getChildren(), function (mnu) {
                                        mnu.onClick = function () {
                                            _this.routeArgs.tab = _this.tabsIdMapping[mnu.id.replace('_stcMi', '')];
                                            _this.routeArgs.md5 = _this.md5;
                                            _this.redirect(null, null, null, _this.routeArgs);
                                        };
                                    });
                                });

                                // Register selectChild event
                                if (registry.byId(this.tabContainer)) {
                                    aspect.after(registry.byId(this.tabContainer), 'selectChild', function () {
                                        _this.selected = registry.byId(_this.tabContainer).selectedChildWidget.id;
                                        _this.onSelected(_this.selected);
                                    });
                                }

                            }
                        },
                        getChildrenArgs: function (childrenId) {
                            return this.routeArgs || {};
                        },
                        activate: function (args) {
                            var _this = this;
                            if (_this.stackView.getChildren().length === 0) {
                                return this.inherited(arguments);
                            }
                            args = args || {insert: true};
                            args.tab = ((args.tab && _this.tabsNameMapping[args.tab]) ? _this.tabsNameMapping[args.tab] : _this[_this.tabToAllwaysActivate].id) || _this[_this.tabToAllwaysActivate].id;
                            _this.stackView.selectChild(args.tab);

                            if (_this.tabToAllwaysActivate) {
                                return _this[_this.tabToAllwaysActivate].activate(_this.getChildrenArgs(_this[_this.tabToAllwaysActivate].id, args)).then(
                                        function () {
                                            if (args.tab !== _this[_this.tabToAllwaysActivate].id) {
                                                registry.byId(args.tab).activate(_this.getChildrenArgs(args.tab, args));
                                            }
                                        },
                                        function (err) {
                                            console.log(err);
                                            //console.log(err.getStack());
                                        }
                                );
                            } else {
                                return registry.byId(args.tab).activate(_this.getChildrenArgs(args.tab, args));
                            }
                        },
                        desactivate: function () {
                            return this.stackView.selectedChildWidget.desactivate();
                        }
                    }
            );
        });