define(
        [
            "dojo/_base/kernel",
            "dojo/_base/declare",
            "dojo/_base/array",
            "dojo/_base/json",
            "dojo/on",
            "dojo/has",
            "dojo/topic",
            "dojo/window",
            "dojo/Deferred",
            "dojo/hash",
            "doze/router/module",
            "dijit/Dialog",
            "dijit/registry"
        ],
        function (
                kernel, declare, array, json, on, has, topic, winUtils, Deferred,
                hash, router, Dialog, registry
                ) {
            return declare("doze.widget._routedMixin", [], {
                hash: '',
                anchor: '',
                routeTargetId: '',
                routeModule: '',
                routeController: '',
                routeView: '',
                md5: '',
                routeArgs: null,
                postMixInProperties: function () {
                    this.routeArgs = this.routeArgs || {};
                    var _this = this;
                    this.watch('label', function () {
                        router.changeLabel(_this);
                    });
                },
                activate: function (args) {
                    var d = new Deferred();
                    d.resolve({});
                    return d.promise;
                },
                desactivate: function () {
                    return true;
                },
                _open: function (view, controller, module, args, targetId, redirection) {
                    view = view || this.routeView;
                    controller = controller || this.routeController;
                    module = module || this.routeModule;
                    targetId = targetId || this.routeTargetId;
                    args = args || {};
                    return router.open(view, controller, module, args, targetId, redirection);
                },
                assemble: function (view, controller, module, args, targetId) {
                    kernel.deprecated("assemble", "Use openURL instead (see https://doc.amstergroup.com/display/DOZE/Define+a+link+to+another+page+or+open+another+page)", "1.0");

                    view = view || this.routeView;
                    controller = controller || this.routeController;
                    module = module || this.routeModule;
                    targetId = targetId || this.routeTargetId;
                    args = args || {};
                    return router.assemble(view, controller, module, args, targetId);
                },
                openURL: function (view, controller, module, args, targetId) {
                    view = view || this.routeView;
                    controller = controller || this.routeController;
                    module = module || this.routeModule;
                    targetId = targetId || this.routeTargetId;
                    args = args || {};
                    return router.openURL(view, controller, module, args, targetId);
                },
                redirect: function (view, controller, module, args, targetId) {
                    return this._open(view, controller, module, args, targetId, true);
                },
                forward: function (view, controller, module, args, targetId) {
                    return this._open(view, controller, module, args, targetId, false);
                },
                open: function (view, controller, module, args, targetId) {
                    return this._open(view, controller, module, args, targetId, false);
                },
                closeParent: function () {
                    router.close(this.routeTargetId);
                },
                _setRouteTargetIdAttr: function (value) {
                    if (value && registry.byId(value) && registry.byId(value) instanceof Dialog) {
                        var _this = this, _label = '';
                        this.watch('label', function (prop, oldValue, newValue) {
                            _label = '<span class="glyph ' + _this.iconClass + '" ></span><span>' + newValue + '</span>';
                            registry.byId(value).set('title', _label);
                        });
                        _label = '<span class="glyph ' + _this.iconClass + '" ></span><span>' + _this.label + '</span>'
                        registry.byId(value).set('title', _label);
                    }
                    this._set('routeTargetId', value);
                }
            });
        });