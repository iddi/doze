define(
        [
            "dojo/_base/declare",
            "dojo/_base/lang",
            "dojo/has",
            "dojo/on",
            "dojo/Deferred",
            "dojo/dom-geometry",
            "dijit/_WidgetBase",
            "dijit/_TemplatedMixin",
            "dijit/_WidgetsInTemplateMixin",
            "dojo/Evented",
            "./_routedMixin",
            "./_requireAt"
        ],
        function (
                declare, lang, has, on, Deferred, domGeometry,
                _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented, _routedMixin
                ) {
            // Noop function, needed for _trackError when callback due to a bug in 1.8
            // (see http://bugs.dojotoolkit.org/ticket/16667)
            function noop(value) {
                return value;
            }

            return declare("doze.widget.view", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Evented, _routedMixin], {
                templateString: '<div></div>',
                onlyOneInstance: true,
                iconClass: '',
                label: '',
                dirty: false,
                _trackError: function (func) {
                    // summary:
                    //		Utility function to handle emitting of error events.
                    // func: Function|String
                    //		A function which performs some store operation, or a String identifying
                    //		a function to be invoked (sans arguments) hitched against the instance.
                    //		If sync, it can return a value, but may throw an error on failure.
                    //		If async, it should return a promise, which would fire the error
                    //		callback on failure.
                    // tags:
                    //		protected

                    var result;

                    if (typeof func == "string") {
                        func = lang.hitch(this, func);
                    }

                    try {
                        result = func();
                    } catch (err) {
                        // report sync error
                        //emitError.call(this, err);
                        this._emitError(err);
                    }

                    // wrap in when call to handle reporting of potential async error
                    return Deferred.when(result, noop, lang.hitch(this, emitError));
                },
                _emitError: function (err) {
				console.log(err);
                    if (typeof err !== "object") {
                        // Ensure we actually have an error object, so we can attach a reference.
                        err = new Error(err);
						
                    } else if (err.dojoType === "cancel") {
                        // Don't fire dgrid-error events for errors due to canceled requests
                        // (unfortunately, the Deferred instrumentation will still log them)
                        return;
                    }

					if (err && err.response && err.response.data) {
						if (typeof err.response.data === 'string') {
							try {
								var r = JSON.parse(err.response.data, true);
							 } catch (e) {
								var r=[];
								r['error-message'] = 'Invalid response from the server: '+evt.error.response.data;
							}
							
						} else {
							r=err.response.data;
						}
						/* for .NET */
						if (r['Message']) {
							var msg='';
					     
							msg = r['Message'];
							/* get message from last level */
							while (r['InnerException']) {
								r = r['InnerException'];
							}
							if (r['ExceptionMessage']) {
								msg += '<br />' + r['ExceptionMessage'];
							}
								
                                /*
                                 msg = r['Message'];
                                 if (r['ExceptionMessage']) {
                                 msg += '<br />'+r['ExceptionMessage'];
                                 }
                                 if (r['ExceptionType']) {
                                 msg += '<br />'+r['ExceptionType'];
                                 }
                                 if (r['StackTrace']) {
                                 msg += '<br />'+r['StackTrace'].replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br />$2');
                                 }
                                 */
							 r['error-message'] = msg;
						} 
						err.response.data=r;						
					} else if (err.message) {						
						// error js
						var r=[];
						r['error-message'] = (err.name + ": " + err.message);
						if (err.response) {
							err.response.data=r;
						} else {
							err={response:{data:r}};
						}
					} else {
						var r=err;
						err=[];
						err['response']=[];
						err['response']['data']='Internal error: '+r.toString();												
					}
					
                    if (on.emit(this.domNode, "doze-error", {
                        error: err,
                        cancelable: true,
                        bubbles: true})) {
                        console.error(err);
                    }
//					emitError.call(this, err);
                },
                _reemitError: function (err) {
                    this._emitError(err.error);
                },
                startup: function () {
                    // This code needed for ticket 14423 is using removeRepeatNode on a repeat to work with mobile.lists
                    // this.select and this.onCheckStateChanged are called by ListItem so they need to be set
                    // but it seems like a bit of a hack.
                    var parent = null;
                    if (lang.isFunction(this.getParent)) {
                        if (this.getParent() && this.getParent().removeRepeatNode) {
                            this.select = this.getParent().select;
                            this.onCheckStateChanged = this.getParent().onCheckStateChanged;
                        }
                    }
                    this.inherited(arguments);
                },
                _setTargetAttr: function (/*dojo/Stateful*/ value) {
                    // summary:
                    //		Handler for calls to set("target", val).
                    // description:
                    //		Sets target and "ref" property so that child widgets can refer to.
                    this._set("target", value);
                    if (this.binding != value) {
                        // The new value not matching to this.binding means that the change is not initiated by ref change.
                        this.set("ref", value);
                    }
                },
                postMixInProperties: function () {
                    this.inherited(arguments);
                    this.set('target', this);
                }
                /*
                 ,resize: function(changeSize, resultSize) {
                 if (changeSize) {
                 domGeometry.setMarginBox(this.domNode, changeSize);
                 }
                 var children = this.getChildren();
                 if(children.length ===1 && typeof children[0].resize === 'function'){
                 children[0].resize(changeSize, resultSize);
                 }
                 }*/


            });
        });