define(
        [
            "dojo/_base/declare",
            "dojo/on",
            "dojo/dom-class",
            "dojo/dom-construct",
            "dojo/aspect",
            "dojo/query",
            "dojo/text!./templates/grid.html",
            "dojo/_base/lang",
            "./view",
            "dgrid/extensions/DijitRegistry",
            "dgrid/OnDemandGrid",
            "dgrid/Selection",
            "put-selector/put",
            "dgrid/extensions/ColumnResizer",
            "dojo/dom",
            "dojo/dom-style",
            "dojo/parser"
        ],
        function (
                declare, on, domClass, domConstruct, aspect, query, template, lang,
                View, DijitRegistry, Grid, Selection, put, ColumnResizer, dom, domStyle,
                parser
                ) {
            var defaultGridClass = declare("doze.widget._defaultgrid", [Grid, Selection, DijitRegistry, ColumnResizer], {
            });
            return declare("doze.widget.grid", [View], {
                gridNode: null,
                grid: null,
                templateString: template,
                columnsDefinition: null,
                storeDefinition: null,
                emptyOnStart: false,
                createGridOnDemand: false,
                _query: {},
                _defaultRenderer: null,
                _viewMode: "grid",
                gridClass: null,
                customRenderer: null,
                postMixInProperties: function () {
                    this.inherited(arguments);
                    this.gridClass = this.gridClass || defaultGridClass;
                },
                postCreate: function () {
                    this.inherited(arguments);
                    if (this.createGridOnDemand === false) {
                        this.createGrid();
                    }
                },
                createGrid: function () {
                    if (this.grid) {
                        return;
                    }

                    var _this = this;

                    // Grid mixin'
                    var gridArgs = lang.mixin({
                        formatterScope: this,
                        getBeforePut: false,
                        pagingDelay: 150,
                        region: "center",
                        selectionMode: "single",
                        columns: this.getColumns(this),
                        loadingMessage: "Loading data...",
                        noDataMessage: "No results found.",
                        allowTextSelection: true,
                        sort: this.get('defaultSort')

                    }, this.gridOptions);
                    this.grid = new this.gridClass(gridArgs, this.gridNode);

                    // Save default grid renderer (classic list view)
                    this._defaultRenderer = this.grid.renderRow;

                    // If a custom renderer has been applied
                    if (this.customRenderer) {
                        put(this.gridNode, ".gallery"); // Put default dbootstrap class for grid view
                        this.grid.set("showHeader", false); // Disable header in grid view
                        this.grid.renderRow = this.customRenderer; // Override default renderRow() grid function
                        _this._setListButton();
                    } else {
                        this.options && this.options.destroy();
                    }

                    // If view mode is set to grid view
                    if (this.options) {
                        this.options.onClick = function (event) {
                            if (_this._viewMode == "grid") {
                                domClass.remove(_this.gridNode, "gallery");
                                _this.grid.renderRow = _this._defaultRenderer;
                                _this.grid.refresh();
                                _this._setGridButton();
                            } else if (_this._viewMode == "list") {
                                put(_this.gridNode, ".gallery"); // Put default dbootstrap class for grid view
                                _this.grid.renderRow = _this.customRenderer; // Override default renderRow() grid function
                                _this.grid.refresh();
                                _this._setListButton();
                            }
                        }
                    }

                    // Register refresh complete event for custom use
                    on(this.grid, 'dgrid-refresh-complete', lang.hitch(this, this._datasLoaded));
                    var _setHeaderTitleAttr = function () {
                        query('.dgrid-header .dgrid-cell', this.grid.domNode).forEach(function (el) {
                            el.setAttribute('title', el.innerText);
                        });
                    };

                    on(this.grid, 'dgrid-select', lang.hitch(this, this._onSelected));
                    on(this.grid, 'dgrid-error', lang.hitch(this, this._reemitError));

                    aspect.after(this.grid, 'renderHeader', lang.hitch(this, _setHeaderTitleAttr));
                    lang.hitch(this, _setHeaderTitleAttr)();
                },
                _onSelected: function (event) {
                    return this.inherited(arguments);
                },
                _datasLoaded: function (event) {
                    return this.inherited(arguments);
                },
                _getStoreAttr: function () {
                    return this.storeDefinition.getStore();
                },
                getColumns: function (scope) {
                    return this.columnsDefinition.getColumns(scope);
                },
                _getDefaultSortAttr: function () {
                    if (this.storeDefinition && this.storeDefinition.hasDefaultSort && this.storeDefinition.defaultSort) {
                        return this.storeDefinition.defaultSort;
                    }
                    return false;
                },
                _getQueryAttr: function () {
                    return this._query;
                },
                _setQueryAttr: function (value) {
                    this._query = value;
                    this.refreshGrid();
                },
                refresh: function () {
                    this.inherited(arguments);
                },
                refreshGrid: function () {
                    if (!this.grid) {
                        return;
                    }

                    this.grid.startup();
                    if (this.grid.get('store')) {
                        if (this.get('query')) {
                            this.grid.set("query", this.get('query'));
                        }
                    } else {
                        /*
                         var _sort = this.get('defaultSort');
                         if(_sort && _sort.length > 1) {
                         this.grid.updateSortArrow(this.get('defaultSort'));
                         }
                         */
                        if (this.get('query')) {
                            this.grid.set('store', this.get('store'), this.get('query'));
                        } else {
                            this.grid.set('store', this.get('store'));
                        }

                    }
                    this.grid.resize();
                },
                activate: function (args) {
                    if (!this.emptyOnStart && this.grid) {
                        this.set('query', args);
                    }
                    return this.inherited(arguments);
                },
                _setListButton: function () {
                    this._viewMode = "grid";
                    if (this.options) {
                        this.options.set("label", "<i class=\"fa fa-list\"></i>");
                        this.grid.set("showHeader", false); // Disable header in grid view
                    }
                },
                _setGridButton: function () {
                    this._viewMode = "list";
                    if (this.options) {
                        this.options.set("label", "<i class=\"fa fa-th-large\"></i>");
                        this.grid.set("showHeader", true); // Disable header in grid view
                    }
                }
            });
        });