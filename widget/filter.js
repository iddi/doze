define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/_base/kernel",
    "dojo/_base/array",
    "dojo/on",
    "dojo/keys",
    "dojo/json",
    "dojo/request/iframe",
    "dojo/dom-construct",
    "dojo/io-query",
    "dojo/topic",
    "doze/widget/view",
    "dojo/i18n!./nls/message",
    "dojo/text!./templates/filter.html",
    "dijit/registry",
    "dijit/form/Button",
    "dijit/Menu",
    "dijit/MenuItem",
    "doze/form/filter/Operator",
    "dijit/form/DropDownButton",
    "doze/form/filter/FormFilter",
    "doze/form/filter/Hidden"
], function (declare, lang, kernel, array, on, keys, json, iframe,
        domConstruct, ioQuery, topic, view, i18n, template,
        registry, Button, Menu, MenuItem, OperatorFilter) {
    //>>pure-amd
    return declare("doze.widget.filter",
            [view],
            {
                i18n: i18n,
                templateString: template,
                dateOptions: {
                    selector: 'date',
                    datePattern: 'yyyy-MM-dd'
                },
                region: "top",
                defaultFilterClass: OperatorFilter,
                filters: null,
                storeDefinitions: null,
                controllerName: '',
                useLastFilter: true,
                _getControllerNameAttr: function () {
                    var _target = (this.storeDefinitions) ? this.storeDefinitions.target : this.declaredClass;
                    return this.controllerName || _target;
                },
                postCreate: function () {
                    var _this = this, _exportMenu, _xlsExport, _presetsMenu;
                    topic.subscribe('manage-filter-closed', lang.hitch(this, this.onPresetsChanges));
                    this.inherited(arguments);
                    this.filters = [];
                    on(this.btnApplyFilter, 'click', lang.hitch(this, this.onApply));

                    _this._fieldsMenu = new Menu({});
                    this.storeDefinitions.getFieldStore().query({}).forEach(function (el) {
                        var menuItem = new MenuItem({label: el.label});
                        _this._fieldsMenu.addChild(menuItem);
                        on(menuItem, 'click', lang.hitch(_this, _this.addOperatorFilter, {definition: el, label: el.label}));
                    });

                    this.fieldsDropButton.set('dropDown', _this._fieldsMenu);
                    _exportMenu = new Menu({});
                    _xlsExport = new MenuItem({label: '<i class="fa fa-file-excel-o" style="color:green"></i> Export to Excel', iconClass: 'icon-dozefile-excel'});
                    _exportMenu.addChild(_xlsExport);
                    on(_xlsExport, 'click', lang.hitch(_this, _this.onExportToExcel));
                    this.exportDropButton.set('dropDown', _exportMenu);
                    this._resetMenu();
                    on(this.presetsDropButton, 'click', lang.hitch(_this, _this.openFilterAdmin));
                    this.presetsDropButton.startup();
                    on(this.filtersNode, 'keypress', function (evt) {
                        if ((evt.charCode || evt.keyCode) === keys.ENTER) {
                            _this.onApply();
                        }
                    });
                },
                _resetMenu: function () {
                    var _this = this, _presetsMenu, _oldMenu;
                    _oldMenu = this.presetsDropButton.get('dropDown');
                    if (_oldMenu)
                        _oldMenu.destroy();
                    _presetsMenu = new Menu({}, domConstruct.create('div'));

                    this.storeDefinitions.getPreferedFilter().query({controller: this.get('controllerName')}).forEach(function (el) {
                        switch (el.scope) {
                            case 'last':
                                _this._lastfilterid = el.id_search_custom_filter;
                                _this.deserialize(json.parse(el.value));
                                if (_this.useLastFilter)
                                    var menuItem = new MenuItem({label: (el.expanded.length > 0 ? el.expanded.substr(0, 100) : 'No criteria'), title: (el.expanded.length > 0 ? el.expanded : 'No criteria')});
                                break;
                            case 'default':
                                _this._defaultfilterid = el.id_search_custom_filter;
                            default:
                                var menuItem = new MenuItem({label: el.label, title: (el.expanded.length > 0 ? el.expanded : 'No criteria')});
                        }
                        if (menuItem)
                            _presetsMenu.addChild(menuItem);
                        on(menuItem, 'click', lang.hitch(_this, _this.onApplyPreset, el));
                    });
                    this.presetsDropButton.set('dropDown', _presetsMenu);
                },
                onPresetsChanges: function (controller) {
                    if (this.get('controllerName') === controller) {
                        this._resetMenu();
                    }
                },
                onApplyPreset: function (args) {
                    this.deserialize(json.parse(args.value));
                },
                onExportToExcel: function () {
                    var _iframe = iframe.create('excel_export', ''),
                            _url = this.storeDefinitions.target.replace(/\/$/, '.xls/') + '?' + ioQuery.objectToQuery(this.get('value'));
                    if (_iframe.contentWindow) {
                        _iframe.contentWindow.location = _url;
                    } else {
                        _iframe.location = _url;
                    }
                },
                openFilterAdmin: function () {
                    kernel.global.current_json_filter = json.stringify(this.serialize());
                    this.open('search', 'manage', 'search-admin-module', {controller: this.get('controllerName')}, 'filtermanage_dlg');
                },
                saveCurrentAsDefault: function () {
                    var _defaultFilter = {
                        value: json.stringify(this.serialize()),
                        id_search_custom_filter: this._defaultfilterid
                    };
                    this.storeDefinitions.getPreferedFilter().put(_defaultFilter);
                },
                saveLastFilter: function () {
                    var _this = this, _lastFilterApplied = {
                        controller: this.get('controllerName'),
                        value: json.stringify(this.serialize()),
                        label: 'Last filter',
                        description: 'Last filter applied',
                        scope: 'last'
                    };
                    if (this._lastfilterid) {
                        lang.setObject('id_search_custom_filter', this._lastfilterid, _lastFilterApplied);
                    }
                    this.storeDefinitions.getPreferedFilter().put(_lastFilterApplied).then(function (item) {
                        _this._lastfilterid = item.id_search_custom_filter;
                    });
                },
                onApply: function () {
                    this.emit('filter_apply', this.get('value'));
                    if (this.useLastFilter)
                        this.saveLastFilter();
                    return;
                },
                onFilterChange: function () {
                    this.emit('filter_change', this.get('value'));
                    return;
                },
                addOperatorFilter: function (args) {
                    return this.addFilter(args);
                },
                addFilterClick: function () {
                    this.addFilter();
                },
                addFilter: function (args) {
                    var filterArgs = args || {},
                            newFilterNode = domConstruct.create('div'),
                            newFilter,
                            FilterClass;
                    this.filtersNode.appendChild(newFilterNode);

                    filterArgs.storeDefinitions = this.storeDefinitions;
                    FilterClass = (filterArgs.declaredClass) ? lang.getObject(filterArgs.declaredClass, false) : this.defaultFilterClass;

                    newFilter = new FilterClass(filterArgs, newFilterNode);
                    on(newFilter, 'remove', lang.hitch(this, function () {
                        this.removeFilter(newFilter);
                    }));
                    on(newFilter, 'change', lang.hitch(this, this.onFilterChange));

                    on(newFilter, 'apply', lang.hitch(this, this.onApply));

                    this.filters.push(newFilter);
                    this.onFilterChange();
                    return true;
                },
                removeFilter: function (filterToRemove) {
                    var newfilters = [], filter = null, filterID = filterToRemove.id;
                    while (typeof (filter = this.filters.shift()) !== 'undefined') {
                        if (filter.id === filterID) {
                            filter.destroyDescendants();
                            filter.destroy();
                        } else {
                            newfilters.push(filter);
                        }
                    }
                    this.filters = newfilters;
                    this.onFilterChange();
                    return true;
                },
                isValid: function () {
                    return true;
                },
                _getValueAttr: function () {
                    var obj = {}, filterArray = array.map(this.filters, function (filter) {
                        return filter.get('value');
                    }).filter(function (value) {
                        return (value) || false;
                    });
                    array.forEach(filterArray, function (value, idx) {
                        lang.setObject('filter_' + idx, [value], obj);
                    });
                    lang.setObject('filter_count', filterArray.length, obj);
                    return obj;
                },
                _setDefaultfilterAttr: function (value) {
                    this.deserialize(value);
                },
                _getJsQueryValueAttr: function () {
                    var obj = {}, filterArray = array.map(this.filters, function (filter) {
                        return filter.serialize();
                    }).filter(function (serialized) {
                        return (serialized.value && serialized.definition.type === 'text') || false;
                    });

                    array.forEach(filterArray, function (filter, idx) {
                        var _fil = filter.value[2];
                        if (filter.value[1] === '1')
                            _fil = _fil;
                        if (filter.value[1] === '2')
                            _fil = '!' + _fil;
                        if (filter.value[1] === '3')
                            _fil = '^' + _fil;
                        if (filter.value[1] === '4')
                            _fil = _fil + '$';

                        lang.setObject(filter.definition.fieldname, new RegExp(_fil, 'i'), obj);
                    });
                    return obj;
                },
                deserialize: function (objs) {
                    var multifilterArgs = lang.clone(objs), filter;
                    while (typeof (filter = this.filters.shift()) !== 'undefined') {
                        filter.destroyDescendants();
                        filter.destroy();
                    }
                    array.forEach(multifilterArgs, function (val) {
                        if (lang.isArray(val)) {
                            this.addFilter({
                                value: val
                            });
                        } else {
                            this.addFilter(val);
                        }
                    }, this);
                },
                serialize: function () {
                    var vals = array.map(this.filters, function (f) {
                        return f.serialize();
                    });
                    vals = array.filter(vals, function (f) {
                        if (f) {
                            return f;
                        }
                    });
                    return vals;
                },
                count: function () {
                    if (this.filters) {
                        return this.filters.length;
                    }

                    return 0;
                }
            }
    );
});