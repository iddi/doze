define(
        [
            "dojo/_base/declare",
            "dojo/text!./templates/search.html",
            "doze/widget/grid",
            "dojo/on",
            "dojo/_base/lang",
            "doze/widget/filter",
            "dijit/form/Button",
            "dojo/i18n!./nls/message",
            "dijit/Dialog",
            "dijit/form/Select",
            "dgrid/OnDemandGrid",
            "dgrid/Keyboard",
            "dgrid/Selection",
            "dijit/layout/LayoutContainer",
            "dojo/dom-construct",
            "dgrid/extensions/DijitRegistry",
            "dojo/store/Memory",
            "dojo/dom-style",
            "dojo/store/JsonRest",
            "dojo/json",
            "dojo/domReady!"

        ],
        function (declare,
                template,
                grid, on, lang,
                Filter, Button, i18n,
                Dialog, Select, Grid,
                Keyboard, Selection, LayoutContainer,
                domconstruct, DijitRegistry,
                Memory, domstyle, JsonStore,
                json
                ) {
            return declare("doze.widget.search", [grid], {
                templateString: template,
                storeDefinition: null,
                autofilter: false,
                emptyOnStart: true,
                useLastFilter: true,
                createFilter: function () {

                    this.filter = new (declare([Filter]))({
                        storeDefinitions: this.storeDefinition,
                        controllerName: this.get('filterName') || this.storeDefinition.target,
                        useLastFilter: this.useLastFilter
                    }, this.filterNode);
                    on(this.filter, 'filter_apply', lang.hitch(this,
                            function (filterValue) {
                                this.set('query', filterValue);
                            }
                    ));
                    // auto filter
                    on(this.filter, 'filter_change', lang.hitch(this, function (filterValue) {
                        try
                        {
                            this.resize();
                        } catch (E) {
                        }
                    }));

                    if (this.autofilter) {
                        on(this.filter, 'filter_change', lang.hitch(this, function (filterValue) {
                            this.set('query', filterValue);
                        }
                        ));
                    }
                },
                _datasLoaded: function (event) {
                    var _this = this;
                    event.results.total.then(function (total) {
                        _this.filter.countNode.innerHTML = '<span class="searchResultCountNodeTotal">' + total + '</span> results';
                    });
                },
                postCreate: function () {
                    this.createFilter();
                    this.inherited(arguments);
                }
            });
        });
