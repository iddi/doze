define(
        [
            "dojo/_base/declare",
            "dojo/on",
            "dojo/dom-class",
            "dojo/dom-construct",
            "dojo/aspect",
            "dojo/query",
            "dojo/text!./templates/grid.html",
            "dojo/_base/lang",
            "./view",
            "dgrid/extensions/DijitRegistry",
            "dgrid/OnDemandGrid",
            "dgrid/Selection",
            "dgrid/extensions/ColumnResizer",
            "put-selector/put",
            "dojo/dom",
            "dojo/dom-style",
            "dojo/parser"
        ],
        function (
                declare, on, domClass, domConstruct, aspect, query, template, lang,
                View,
                DijitRegistry, Grid, Selection, ColumnResizer,
                put, dom, domStyle,
                parser
                ) {
            var defaultGridClass = declare("doze.widget._defaultgrid", [Grid, Selection, DijitRegistry, ColumnResizer], {
            });
            return declare("doze.widget.dgrid", [View], {
                gridNode: null,
                grid: null,
                templateString: template,
                columnsDefinition: null,
                storeDefinition: null,
                emptyOnStart: false,
                createGridOnDemand: false,
                _globalQuery: {},
                _query: {},
                _defaultRenderer: null,
                _viewMode: "grid",
                gridClass: null,
                customRenderer: null,
                postMixInProperties: function () {
                    this.inherited(arguments);
                    this.gridClass = this.gridClass || defaultGridClass;
                },
                postCreate: function () {
                    this.inherited(arguments);
                    if (this.createGridOnDemand === false) {
                        this.createGrid();
                    }
                },
                createGrid: function () {
                    if (this.grid) {
                        return;
                    }

                    var _this = this;

                    // Grid mixin'
                    var gridArgs = lang.mixin({
                        formatterScope: this,
                        getBeforePut: false,
                        pagingDelay: 150,
                        region: "center",
                        selectionMode: "single",
                        columns: this.getColumns(this),
                        loadingMessage: "Loading data...",
                        noDataMessage: "No results found.",
                        allowTextSelection: true,
                        sort: this.get('defaultSort')

                    }, this.gridOptions);
                    this.grid = new this.gridClass(gridArgs, this.gridNode);


                    // Register refresh complete event for custom use
                    on(this.grid, 'dgrid-refresh-complete', lang.hitch(this, this._datasLoaded));
                    var _setHeaderTitleAttr = function () {
                        query('.dgrid-header .dgrid-cell', this.grid.domNode).forEach(function (el) {
                            el.setAttribute('title', el.innerText);
                        });
                    };

                    on(this.grid, 'dgrid-select', lang.hitch(this, this._onSelected));
                    on(this.grid, 'dgrid-error', lang.hitch(this, this._reemitError));

                    aspect.after(this.grid, 'renderHeader', lang.hitch(this, _setHeaderTitleAttr));
                    lang.hitch(this, _setHeaderTitleAttr)();
                },
                _onSelected: function (event) {
                    return this.inherited(arguments);
                },
                _datasLoaded: function (event) {
                    return this.inherited(arguments);
                },
                _getStoreAttr: function () {
                    return this.storeDefinition.getStore();
                },
                getColumns: function (scope) {
                    return this.columnsDefinition.getColumns(scope);
                },
                _getDefaultSortAttr: function () {
                    if (this.storeDefinition && this.storeDefinition.hasDefaultSort && this.storeDefinition.defaultSort) {
                        return this.storeDefinition.defaultSort;
                    }
                    return null;
                },
                _getGlobalQueryAttr: function () {
                    return this._globalQuery;
                },
                _setGlobalQueryAttr: function (value) {
                    this._globalQuery = value;
                    this.refreshGrid();
                },
                _getQueryAttr: function () {
                    return this._query;
                },
                _setQueryAttr: function (value) {
                    this._query = value;
                    this.refreshGrid();
                },
                refresh: function () {
                    this.inherited(arguments);
                },
                refreshGrid: function () {
                    if (!this.grid) {
                        return;
                    }
 
                    this.grid.startup();
                    this.grid.set('collection', this.get('store').filter(
                            lang.mixin(this.get('globalQuery'), this.get('query'))
                            ));
                    this.grid.resize();
                },
                activate: function (args) {
                    if (!this.emptyOnStart && this.grid) {
                        this.set('globalQuery', args);
                    }
                    return this.inherited(arguments);
                }
            });
        });