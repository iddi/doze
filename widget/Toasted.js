define(
        [
            "dojo/_base/declare",
            "dojo/_base/lang",
            "dojox/widget/Toaster",
            "dojo/dom-construct",
            "dojo/_base/window"
        ],
        function (
                declare,
                lang,
                Toaster,
                domConstruct,
                window
                ) {

            var _toaster = new Toaster(
                    {positionDirection: 'br-left'},
            domConstruct.create('div', null, window.body())
                    );
            return declare("doze.widget.Toasted", [], {
                toast: function (message, type) {
                    _toaster.setContent(message, type);
                    _toaster.show();
                }
            });
        });

