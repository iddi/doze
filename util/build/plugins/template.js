define(["dojo/json", "dojo/_base/xhr"], function (json, xhr) {
    return {
        start: function (
                mid,
                referenceModule,
                bc
                ) {

            var result = [bc.amdResources["dojo/require"]];
            mid.split(",").map(function (mid) {
                var module = bc.amdResources[mid];
                var templatePlugin = bc.amdResources["doze/template"],
                        moduleInfo = bc.getSrcModuleInfo(mid, referenceModule, true);

                if (!module) {
                    try {
                        var textResource = readUrl(bc.publicUrl + "template/" + moduleInfo.mid);
                        bc.log("pacify", 'Write template:' + moduleInfo.mid);
                        result.push({
                            pid: moduleInfo.pid,
                            mid: 'template/' + moduleInfo.mid,
                            deps: [],
                            getText: function () {
                                return json.stringify(this.text + "");
                            },
                            text: textResource,
                            internStrings: function () {
                                return ["url:" + this.mid, this.getText()];
                            },
                            dest: referenceModule.dest.replace(referenceModule.mid, moduleInfo.mid) + '.html',
                            encoding: 'utf8'

                        });
                    } catch (e) {
                        bc.log("pacify", e);
                        bc.log("pacify", "Could not load doze module: " + moduleInfo.mid + ' on application: ' + bc.publicUrl + 'with url: ' + bc.publicUrl + "template/" + moduleInfo.mid);
                    }
                } else {
                    result.push(module);
                }
            });
            return result;
        }
    };
});
