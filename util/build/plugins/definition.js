define(function () {
    return {
        start: function (
                mid,
                referenceModule,
                bc
                ) {

            var result = [bc.amdResources["dojo/require"]];
            mid.split(",").map(function (mid) {
                var module = bc.amdResources[mid];
                var definitionPlugin = bc.amdResources["doze/definition"],
                        moduleInfo = bc.getSrcModuleInfo(mid, referenceModule, true);
                if (!module) {
                    try {
                        var textResource = readUrl(bc.publicUrl + "definition/" + moduleInfo.mid + ".js");
                        bc.log("pacify", 'Write definition:' + moduleInfo.mid);
                        result.push({
                            pid: moduleInfo.pid,
                            mid: 'definition/' + moduleInfo.mid,
                            deps: [],
                            tag: {amd: true},
                            getText: function () {
                                return this.text;
                            },
                            text: textResource,
                            dest: referenceModule.dest.replace(referenceModule.mid, moduleInfo.mid),
                            encoding: 'utf8'

                        });
                    } catch (e) {
                        bc.log("pacify", "Could not load doze module: " + moduleInfo.mid + ' on application: ' + bc.publicUrl);
                    }
                } else {
                    result.push(module);
                }
            });
            return result;
        }
    };
});