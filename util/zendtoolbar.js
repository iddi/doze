require([
    "dojo/has",
    "dojo/request/notify",
    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-style"
], function (has, notify, dom, domContruct, domStyle) {
    if (has("doze-developper-tools")) {
        notify("done", function (responseOrError) {
            var toolbar = dom.byId('zend-developer-toolbar');
            if (toolbar)
                var html = responseOrError.getHeader('X-NSI-Tools');
            if (html) {
                domContruct.place(html, toolbar, 'replace')
                domStyle.set(dom.byId('zend-developer-toolbar'), "display", "");
            }
        });
    }
});