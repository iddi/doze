/*
 * Template text converter
 * 
 * Usage :
 *  - Load your template in your module with dojo/text
 *  - Give the template text and your object for binding into the template
 *  
 *  Properties in the template must be like the following :
 *  
 *  {{myObjectProperty}}
 *  
 *  Custom arguments can also be supplied in a JSON format
 *  
 *  {
 *      "myCustomArg":"This is a custom arg",
 *      "myDatetime": new Date()
 *  }
 * 
 * @returns template text with parsed values
 */
define([], function () {
    return {
        /* 
         * Private method to replace fields into the template
         *  Regex is used to replace all field of the same name
         */
        _replace: function (tpl, key, value) {
            var regex = new RegExp("{{" + key + "}}", "g");
            return tpl.replace(regex, value);
        },
        /*
         * View template converter
         * Return template text with parsed values
         */
        convert: function (template, obj, options) {
            var tmp = template;

            for (var key in obj) {
                tmp = this._replace(tmp, key, obj[key]);
            }
            ;

            // Insert custom data
            if (options !== null) {
                for (var key in options) {
                    tmp = this._replace(tmp, key, options[key]);

                    // Replace every null value
                    if (key == "null") {
                        var regex = new RegExp(key, "g");
                        tmp = tmp.replace(regex, options[key]);
                    }
                }
                ;
            }
            return tmp;
        }
    };
});