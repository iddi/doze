define([
    "dojo/_base/array", // array.forEach array.indexOf array.some
    "dojo/cookie", // cookie
    "dojo/_base/declare", // declare
    "dojo/dom", // dom.setSelectable
    "dojo/dom-class", // domClass.add
    "dojo/dom-construct", // domConstruct.create domConstruct.destroy
    "dojo/dom-geometry", // domGeometry.marginBox domGeometry.position
    "dojo/dom-style", // domStyle.style
    "dojo/_base/event", // event.stop
    "dojo/_base/kernel", // kernel.deprecated
    "dojo/_base/lang", // lang.extend lang.hitch
    "dojo/on",
    "dojo/sniff", // has("mozilla")
    "dijit/layout/_LayoutWidget"
], function (array, cookie, declare, dom, domClass, domConstruct, domGeometry, domStyle,
        event, kernel, lang, on, has, _LayoutWidget) {

    var AdaptiveFormContainer = declare("doze.layout.AdaptiveFormContainer", _LayoutWidget, {
        resize: function () {
            var contentBox = domGeom.getContentBox(this.domNode);

            var unit = 64;
            var margin = 10;
            var labelwidth = 136;
            var rangeseparator = 5;
            var mincolumnwidth = 360;
            var minwidth = 660;
            if (contentBox.w) {
                var width = contentBox.w;
                console.log(width);
                /*
                 var num = 6;
                 if (width > minwidth) {
                 
                 var columnwidth = ((((width - (35 * margin)) / 16) * num) +
                 (margin * 2 * (num - 1)));
                 
                 query('.columns.c6', this.domNode).forEach(function(node) {
                 domStyle.set(node, {'width': columnwidth + 'px'});
                 });
                 } else {
                 var columnwidth = (width - 4 * margin);
                 query('.columns.c6', this.domNode).forEach(function(node) {
                 domStyle.set(node, {'width': columnwidth + 'px', 'margin': 0});
                 });
                 }
                 */
                for (var num = 1; num <= 16; num++) {
                    console.log(num);
                    if (width > minwidth) {
                        var columnwidth = ((((width - (33 * margin)) / 16) * num) +
                                (margin * 2 * (num - 1)));

                        query('.columns.c' + num, this.domNode).forEach(function (node) {
                            domStyle.set(node, {'width': columnwidth + 'px'});
                        });
                    } else {
                        var columnwidth = (width - (2 * margin));
                        query('.columns.c' + num, this.domNode).forEach(function (node) {
                            domStyle.set(node, {'width': columnwidth + 'px', 'margin': 0});
                        });
                    }
                    console.log(columnwidth);

                    if (columnwidth < mincolumnwidth) {
                        query('.columns.c' + num + ' .generate-row label', this.domNode).forEach(function (node) {
                            domStyle.set(node, {'width': (columnwidth - 10) + 'px', 'margin': 0, textAlign: 'left'});
                        });

                        query('.columns.c' + num + ' .generate-row .generate-dijit-cell.dijitTextBox', this.domNode).forEach(function (node) {
                            domStyle.set(node, {'width': columnwidth + 'px', 'margin': 0});
                        });
                    } else {
                        query('.columns.c' + num + ' .generate-row label', this.domNode).forEach(function (node) {
                            domStyle.set(node, {'width': labelwidth + 'px', textAlign: 'right'});
                        });

                        query('.columns.c' + num + ' .generate-row .generate-dijit-cell.dijitTextBox', this.domNode).forEach(function (node) {
                            domStyle.set(node, {'width': columnwidth - 10 - labelwidth + 'px'});
                        });
                    }

                }
            }
            return this.inherited(arguments);

        }
    });
    return AdaptiveFormContainer;

});
                        