define([
    "dojo/_base/declare",
    "dojo/on",
    "dojo/_base/lang",
    "dojo/query",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojox/widget/Toaster",
    "dojo/topic",
    "dojo/json"
], function (
        declare,
        on,
        lang,
        query,
        domConstruct,
        domStyle,
        Toaster,
        topic,
        JSON
        ) {
    return declare(
            null,
            {
                toaster: null,
                defaultErrorHandling: function (evt) {
                    var msg = '';
                    if (evt.error && evt.error.response && evt.error.response.data) {
                        console.log(evt.error);
                        try {
                      	    if (typeof evt.error.response.data === 'object') {
								var r=evt.error.response.data;
							} else {
                                var r = JSON.parse(evt.error.response.data, true);
							}
							console.log(r);
                            if (r['error-message']) {
                                /* for PHP */
                                msg = r['error-message'];
                            }  else {
								
								if (r['error']) {
									/* for .NET OData */
									r=r['error'];
									msg = r['message'];
									 while (r['innererror']) {
										r = r['innererror'];
									}
									if (r['message']) {
										msg += '<br />' + r['message'];
									}
								} else {
									/* for .NET web api*/
									
									msg = r['Message'];
									
									/* get message from last level */
									
									while (r['InnerException']) {
										r = r['InnerException'];
									}
									if (r['ExceptionMessage']) {
										msg += '<br />' + r['ExceptionMessage'];
									}
								}
                                /*
                                 msg = r['Message'];
                                 if (r['ExceptionMessage']) {
                                 msg += '<br />'+r['ExceptionMessage'];
                                 }
                                 if (r['ExceptionType']) {
                                 msg += '<br />'+r['ExceptionType'];
                                 }
                                 if (r['StackTrace']) {
                                 msg += '<br />'+r['StackTrace'].replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br />$2');
                                 }
                                 */
                            }
                        } catch (e) {
                            msg = 'Invalid response from the server';
                        }
                    }
                    if (!msg) {
                        msg = evt.error;
                    }
                    if (!msg) {
                        msg = 'Unknown error occurs';
                    }
                    console.log(msg);
                    topic.publish("toastErrorMessage", {
                        message: msg,
                        type: "error"
                    });
//                      console.error(evt.error);         
                },
                postCreate: function () {
                    var d = domConstruct.create('div');
                    document.body.appendChild(d);
                    this.toaster = new Toaster({
                        messageTopic: 'toastErrorMessage',
                        positionDirection: "tr-down",
                        duration: 0
                    }, d);
                    var _this = this;
                    on(this.domNode, "doze-error", function (evt) {
                        _this.defaultErrorHandling(evt);
                    });
                    on(this.domNode, "dgrid-error", function (evt) {
                        _this.defaultErrorHandling(evt);
                    });
                    this.inherited(arguments);
                }
            });
});
