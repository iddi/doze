define(["dojo/text", "dojo/_base/lang"], function (text, lang) {
    var templatePlugin = {
        normalize: function (id, toAbsMid) {
            // id is something like (path may be relative):
            //
            //	 "path/to/text.html"
            //	 "path/to/text.html!strip"
            var parts = id.split("!"),
                    url = parts[0];
            var norm = (/^\./.test(url) ? toAbsMid(url) : url) + (parts[1] ? "!" + parts[1] : "");
            norm = (norm.substr(0, 4) === 'app_') ? norm.substr(4) : norm;
            return 'template/' + norm;
        }
    }
    return lang.mixin(text, templatePlugin);
});

