define([
    "dojo/_base/xhr", 
	'dojo/request',
    'dojo/when',
    'dojo/_base/lang',
    'dojo/_base/array',
    'dojo/json',
    'dojo/_base/declare',
    'dstore/Store',
    'dstore/QueryResults',
    'dstore/QueryMethod',
    'doze/dstore/ODataFilter'
], function (xhr, request, when, lang, arrayUtil, JSON, declare, Store, QueryResults, QueryMethod, ODataFilter) {

    var push = [].push;

    return declare(Store, {
        Filter: ODataFilter,
        // summary:
        //		This is a basic store for RESTful communicating with a server through JSON
        //		formatted data. It extends dstore/Store.

        constructor: function () {
            // summary:
            //		This is a basic store for RESTful communicating with a server through JSON
            //		formatted data.
            // options: dstore/JsonRest
            //		This provides any configuration information that will be mixed into the store
            this.headers || (this.headers = {});

            this._targetContainsQueryString = this.target.lastIndexOf('?') >= 0;
        },
        // headers: Object
        //		Additional headers to pass in all requests to the server. These can be overridden
        //		by passing additional headers to calls to the store.
        headers: {},
        // parse: Function
        //		This function performs the parsing of the response text from the server. This
        //		defaults to JSON, but other formats can be parsed by providing an alternate
        //		parsing function. If you do want to use an alternate format, you will probably
        //		want to use an alternate stringify function for the serialization of data as well.
        //		Also, if you want to support parsing a larger set of JavaScript objects
        //		outside of strict JSON parsing, you can provide dojo/_base/json.fromJson as the parse function
        parse: JSON.parse,
        // stringify: Function
        //		This function performs the serialization of the data for requests to the server. This
        //		defaults to JSON, but other formats can be serialized by providing an alternate
        //		stringify function. If you do want to use an alternate format, you will probably
        //		want to use an alternate parse function for the parsing of data as well.
        stringify: JSON.stringify,
        // target: String
        //		The target base URL to use for all requests to the server. This string will be
        //		prepended to the id to generate the URL (relative or absolute) for requests
        //		sent to the server
        target: '',
        // _targetContainsQueryString: Boolean
        //		A flag indicating whether the target contains a query string

		expandParam: '$expand',

		// array with the fields to be expanded through m-n relationship of the odata model
		expandAttr: null,
		
        sortParam: '$orderby',
        //		The query parameter to used for holding sort information. If this is omitted, than
        //		the sort information is included in a functional query token to avoid colliding
        //		with the set of name/value pairs.

        // ascendingPrefix: String
        //		The prefix to apply to sort property names that are ascending
        ascendingPrefix: ' asc',
        // descendingPrefix: String
        //		The prefix to apply to sort property names that are ascending
        descendingPrefix: ' desc',
        // accepts: String
        //		Defines the Accept header to use on HTTP requests
        accepts: "application/json; odata=minimal",
        // rangeStartParam: String
        //		The indicates if range limits (start and end) should be specified
        //		in a query parameter, and what the start parameter should be.
        //		This must be used in conjunction with the rangeCountParam
        //		If this is not specified, the range will
        //		included with a RQL style limit() parameter
        // rangeCountParam: String
        //		The indicates if range limits (start and end) should be specified
        //		in a query parameter, and what the count parameter should be.
        //		This must be used in conjunction with the rangeStartParam
        //		If this is not specified, the range will
        //		included with a RQL style limit() parameter

        childAttr: "children",
        autoEmitEvents: false, // this is handled by the methods themselves

        _getQuotedId: function(id){
            var _quoted;
            if (typeof id === 'number' || /[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12}/i.test(id)) {
                _quoted = id + '';
            } else {
                _quoted = '\'' + id + '\'';
            }
            
            return _quoted;
        },
		_getItemTarget: function(id){
			return this.target + "(" + this._getQuotedId(id) + ")";
		},
        get: function (id, options) {
            // summary:
            // Retrieves an object by its identity. This will trigger a
            // GET request to the server using the url `this.target +
            // "("+id+")"`.
            // id: String
            // The identity to use to lookup the object
            // options: Object?
            // HTTP headers. For consistency with other methods, if a
            // `headers` key exists on this object, it will be used to
            // provide HTTP headers instead.
            // returns: Object
            // The object in the store that matches the given id.
            options = options || {};
            var headers = lang.mixin({Accept: this.accepts}, this.headers, options.headers || options);
            var store = this;
            return request(this._getItemTarget(id), {
                headers: headers
            }).then(function (response) {
                return store._restore(store.parse(response), true);
            });
        },
		callFunction: function (target,options){
			options = options || {};
            var headers = lang.mixin({Accept: this.accepts}, this.headers, options.headers || options);
            return request(target, {
                headers: headers
            });
		},
		callAction: function (target,object,options){
			options = options || {};
            var headers = lang.mixin({Accept: this.accepts}, this.headers, options.headers || options);
            /*
            return request(target, {
                method: 'POST',
				data: this.stringify(object),
				headers: headers
            });
			*/
			return xhr('POST', {
                                url: target,
                                postData: JSON.stringify(object),
                                //handleAs: "json",
                                headers: {
                                    "Content-Type": "application/json"
                                }
                            })
		},
        put: function (object, options) {
            // summary:
            //		Stores an object. This will trigger a PUT request to the server
            //		if the object has an id, otherwise it will trigger a POST request.
            // object: Object
            //		The object to store.
            // options: __PutDirectives?
            //		Additional metadata for storing the data.  Includes an 'id'
            //		property if a specific id is to be used.
            // returns: dojo/_base/Deferred
            options = options || {};
            var id = ('id' in options) ? options.id : this.getIdentity(object);
            var hasId = typeof id !== 'undefined';
            var store = this;
			
			
			if (this.expandAttr) {
				for(var key in object){
					console.log("key=" + key + " value=" + object[key]);
					
					if (arrayUtil.indexOf(this.expandAttr, key) >= 0) {
						object['id_sponsor']=object[key]['id'];  
						// TODO replace id_store et id  or 
						// remove id_sponsor and add  "Sponsor@odata.bind":["http://host/service/Sponsor(123)"]
						delete object[key];
					}
				}
            }
			
            var headers = lang.mixin({
                "Content-Type": "application/json; odata.metadata=minimal",
                Accept: this.accepts
            }, this.headers, options.headers);

            if (hasId) {
                headers["IF-MATCH"] = options.overwrite ? "*" : (options.etag || "*")
            }

            var initialResponse = request(hasId ? (this.target + "(" + this._getQuotedId(id) + ")") : this.target, {
                method: hasId ? 'PUT' : 'POST',
                data: this.stringify(object),
                headers: lang.mixin(headers, this.headers, options.headers)
            });

            return initialResponse.then(function (response) {
                var event = {};

                if ('beforeId' in options) {
                    event.beforeId = options.beforeId;
                }
                
                var result = event.target = (response === '') ? object : store._restore(store.parse((response)), true) || object;
				
                when(initialResponse.response, function (httpResponse) {
                    store.emit(httpResponse.status === 201 /* created */ ? 'add' : 'update', event);
                });

                return result;
            });
        },
		
        add: function (object, options) {
            // summary:
            //		Adds an object. This will trigger a PUT request to the server
            //		if the object has an id, otherwise it will trigger a POST request.
            // object: Object
            //		The object to store.
            // options: __PutDirectives?
            //		Additional metadata for storing the data.  Includes an 'id'
            //		property if a specific id is to be used.
            options = options || {};
            options.overwrite = false;
            return this.put(object, options);
        },
        remove: function (id, options) {
            // summary:
            //		Deletes an object by its identity. This will trigger a DELETE request to the server.
            // id: Number
            //		The identity to use to delete the object
            // options: __HeaderOptions?
            //		HTTP headers.
            options = options || {};
            var store = this;
            var id = ('id' in options) ? options.id : id;
           
            
            return request(this.target + "(" + this._getQuotedId(id) + ")", {
                method: 'DELETE',
                headers: lang.mixin({}, this.headers, options.headers)
            }).then(function (response) {
                var target = response && store.parse(response);
                store.emit('delete', {id: id, target: target});
                return response ? target : true;
            });
        },
        fetch: function () {
            var results = this._request();
            return new QueryResults(results.data, {
                response: results.response
            });
        },
        fetchRange: function (kwArgs) {
            var start = kwArgs.start,
                    end = kwArgs.end,
                    requestArgs = {};
            requestArgs.queryParams = this._renderRangeParams(start, end);

            var results = this._request(requestArgs);
            return new QueryResults(results.data, {
                totalLength: results.total,
                response: results.response
            });
        },
        filter: new QueryMethod({
            type: 'filter',
            normalizeArguments: function (filter) {
                var Filter = this.Filter;
                if (filter instanceof ODataFilter) {
                    return [filter];
                }
                return [new ODataFilter(filter)];
            }
        }),
        select: new QueryMethod({
            type: 'select',
            normalizeArguments: function (fields) {
                return [fields];
            }
        }),
        _request: function (kwArgs) {
            kwArgs = kwArgs || {};

            // perform the actual query
            var headers = lang.delegate(this.headers, {Accept: this.accepts});

            if ('headers' in kwArgs) {
                lang.mixin(headers, kwArgs.headers);
            }

            var queryParams = this._renderQueryParams(),
                    requestUrl = this.target;

            if ('queryParams' in kwArgs) {
                push.apply(queryParams, kwArgs.queryParams);
            }

            if (queryParams.length > 0) {
                requestUrl += (this._targetContainsQueryString ? '&' : '?') + queryParams.join('&');
            }

            var response = request(requestUrl, {
                method: 'GET',
                headers: headers
            });
            var collection = this;
            return {
                data: response.then(function (response) {
                    var results = collection.parse(response);
                    // support items in the results
                    results = results.value || results;
                    for (var i = 0, l = results.length; i < l; i++) {
                        results[i] = collection._restore(results[i], true);
                    }
                    return results;
                }),
                total: response.then(function (response) {
                    var results = collection.parse(response);
                    return results['@odata.count'];
                }),
                response: response.response
            };
        },
        _renderFilterParams: function (filter, chidren) {
            // summary:
            //		Constructs filter-related params to be inserted into the query string
            // returns: String
            //		Filter-related params to be inserted in the query string

            var type = filter.type;
            var args = filter.args;
            if (!type) {
                return [''];
            }
            if (type === 'string') {
                return [args[0]];
            }
            if (type === 'and' || type === 'or') {
                return [((chidren !== true) ? '$filter=' : '') + arrayUtil.map(filter.args, function (arg) {
                        // render each of the arguments to and or or, then combine by the right operator
                        var renderedArg = this._renderFilterParams(arg, true);
                        return ((arg.type === 'and' || arg.type === 'or') && arg.type !== type) ?
                                // need to observe precedence in the case of changing combination operators
                                '(' + renderedArg + ')' : renderedArg;
                    }, this).join(' ' + type + ' ')];
            }
            var _filterString = '';
            switch (type) {
                case 'contains':
                case 'startswith':
                case 'endswith':
                    _filterString = ' ' + type + '(' + encodeURIComponent(args[0]) + ',\'' + encodeURIComponent(args[1]) + '\')';
                    break;
                default:
                    _filterString = encodeURIComponent(args[0]) + ' ' + type + ' ' + encodeURIComponent(args[1]);
                    break;
            }
            if (chidren === true) {
                return _filterString;
            } else {
                return ["$filter=" + _filterString];
            }

        },
        _renderSelectParams: function (select) {
            // summary:
            //		Constructs sort-related params to be inserted in the query string
            // returns: String
            //		Sort-related params to be inserted in the query string

            var selectString = arrayUtil.map(select, function (selectOption) {
                return encodeURIComponent(selectOption);
            }, this);

            var params = [];
            if (selectString) {
                params.push('$select=' + selectString.join(','));
            }
            return params;
        },
        _renderSortParams: function (sort) {
            // summary:
            //		Constructs sort-related params to be inserted in the query string
            // returns: String
            //		Sort-related params to be inserted in the query string

            var sortString = arrayUtil.map(sort, function (sortOption) {
                var prefix = sortOption.descending ? this.descendingPrefix : this.ascendingPrefix;
                return encodeURIComponent(sortOption.property) + prefix;
            }, this);

            var params = [];
            if (sortString) {
                params.push(encodeURIComponent(this.sortParam) + '=' + sortString.join(','));
            }
            return params;
        },
        _renderExpandParams: function () {
            // summary:
            //		Constructs expand-related params to be inserted in the query string
            // returns: String
            //		Expand-related params to be inserted in the query string

			var params = [];
            if (this.expandAttr) {
                params.push(encodeURIComponent(this.expandParam) + '=' + encodeURIComponent(this.expandAttr.join(',')));
            }
            return params;
        },
        _renderRangeParams: function (start, end) {
            // summary:
            //		Constructs range-related params to be inserted in the query string
            // returns: String
            //		Range-related params to be inserted in the query string
            var params = [];
            var count = end - start;
            if (start >= 0 || count >= 0) {
                params.push(
                        "$skip=" + (start || 0),
                        "$count=true");
                if (count != Infinity) {
                    params.push("$top=" + count);
                }
            }

            return params;
        },
        _renderQueryParams: function () {
            var queryParams = [];
            arrayUtil.forEach(this.queryLog, function (entry) {
                var type = entry.type,
                        renderMethod = '_render' + type[0].toUpperCase() + type.substr(1) + 'Params';

                if (this[renderMethod]) {
                    push.apply(queryParams, this[renderMethod].apply(this, entry.normalizedArguments));
                } else {
                    console.warn('Unable to render query params for "' + type + '" query', entry);
                }
            }, this);
			
			push.apply(queryParams, this['_renderExpandParams'].apply(this));

            return queryParams;
        },
        _renderUrl: function () {
            // summary:
            //		Constructs the URL used to fetch the data.
            // returns: String
            //		The URL of the data

            var queryParams = this._renderQueryParams();
            var url = this.target;
            if (queryParams.length > 0) {
                url += '?' + queryParams.join('&');
            }
            return url;
        }
    });
});