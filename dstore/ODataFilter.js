define(['dojo/_base/declare'], function (declare) {
    // a Filter builder
    function filterCreator(type) {
        // constructs a new filter based on type, used to create each method
        return function newFilter() {
            var ODataFilter = this.constructor;
            var filter = new ODataFilter();
            filter.type = type;
            filter.args = arguments;
            if (this.type) {
                // we are chaining, so combine with an and operator
                return filterCreator('and').call(ODataFilter.prototype, this, filter);
            }
            return filter;
        };
    }
    var ODataFilter = declare(null, {
        constructor: function (filterArg) {
            var argType = typeof filterArg;
            switch (argType) {
                case 'object':
                    var filter = this;
                    // construct a filter based on the query object
                    for (var key in filterArg) {
                        var value = filterArg[key];
                        if (value instanceof this.constructor) {
                            // fully construct the filter from the single arg
                            filter = filter[value.type](key, value.args[0]);
                        } else if (value && value.test) {
                            // support regex
                            var value = value + "";
                            var i = value.indexOf("*");
                            // Handle wildcards
                            if (i != -1) {
                                value = value.slice(i != 0 ? 0 : 1, value.length - (i != 0 ? 1 : 0));
                                if (i === 0) {
                                    i = value.indexOf("*");
                                    if (i != -1) {
                                        value = value.slice(i != 0 ? 0 : 1, value.length - (i != 0 ? 1 : 0));
                                        filter = filter.contains(key, value);
                                    } else {
                                        filter = filter.endswith(key, value);
                                    }
                                } else {
                                    filter = filter.startswith(key, value);
                                }
                            }
                        } else {
                            filter = filter.eq(key, value);
                        }
                    }
                    this.type = filter.type;
                    this.args = filter.args;
                    break;
                case 'function': case 'string':
                    // allow string and function args as well
                    this.type = argType;
                    this.args = [filterArg];
            }
        },
        // define our operators
        and: filterCreator('and'),
        or: filterCreator('or'),
        eq: filterCreator('eq'),
        ne: filterCreator('ne'),
        lt: filterCreator('lt'),
        le: filterCreator('lte'),
        gt: filterCreator('gt'),
        ge: filterCreator('gte'),
        contains: filterCreator('contains'),
        startswith: filterCreator('startswith'),
        endswith: filterCreator('endswith')
    });
    ODataFilter.filterCreator = filterCreator;
    return ODataFilter;
});