define([
    "dojo/text",
    "dojo/_base/lang"
], function (text, lang) {
    //>>pure-amd
    var templatePlugin = {
        normalize: function (id, toAbsMid) {
            var parts = id.split("!"),
                    url = parts[0],
                    norm = (/^\./.test(url) ? toAbsMid(url) : url) + (parts[1] ? "!" + parts[1] : "");
            norm = (norm.substr(0, 4) === 'app_') ? norm.substr(4) : norm;
            return 'store/' + norm;
        }
    };
    return lang.mixin(text, templatePlugin);
});

