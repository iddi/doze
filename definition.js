define([
], function () {
    //>>pure-amd
    return {
        dynamic: true,
        normalize: function (id, toAbsMid) {
            var parts = id.split("!"),
                    url = parts[0];

            return 'definition/' + ((/^\./.test(url) ? toAbsMid(url) : url) + (parts[1] ? "!" + parts[1] : ""));
        },
        load: function (mid, require, loaded) {
            require([mid], function (mod) {
                loaded(mod);
            });
        }
    };
});

