define(
        [
            "dojo/_base/kernel",
            "dojo/has",
            "dojo/router",
            "dojo/_base/json",
            "dojo/_base/array",
            "dojo/hash",
            "dijit/registry",
            "dojo/_base/lang",
            "dojo/topic",
            "dijit/Dialog",
            "dojo/on",
            "dojox/encoding/digests/MD5",
            "dojox/widget/Standby"
                    //"doze/log/logger"
//         ,   "./history/store" moved into history.store
        ],
        function (kernel, has, router, json, array, hash, registry, lang, topic, Dialog, on, MD5, Standby, /*logger,*/ historyStore) {
            var _views = [],
                    regexpView = /([^{#|\/]+\/)?([^{#|\/]+)\/([^{#|\/]+)\/([^{#|\/]+)({[^}]+})?/g,
                    loadView = function (viewHash, module, controller, view, params, target) {
                        var args = (typeof params !== 'object') ? (params ? json.fromJson(params) : null) : params,
                                routeTargetId = (target || '').replace('/', ''),
                                dlg;
                        if (routeTargetId !== '') {
                            if (!registry.byId(routeTargetId)) {
                                dlg = new Dialog({"class": 'widget', "id": routeTargetId});
                                on(dlg, 'hide', function () {
                                    _close(routeTargetId);
                                });
                            }
                        }
                        target = registry.byId(routeTargetId) || registry.byId('centercontent');
                        routeTargetId = target.id;
                        var requireUrl = module + '/' + controller + '/' + view;

                        var existing = array.filter(target.getChildren(), function (c) {
                            return c.get('anchor') === requireUrl;
                        });

                        var _afterActivation = function (viewInstance, doNotAddInHistory) {
                            //router_standby.hide();
                            /*
                             registry.byClass('dijit.layout.TabContainer').forEach(function(el) {
                             el.resize();
                             });
                             */
                            if (doNotAddInHistory) {
                                return;
                            }

                            var md5 = (args && args.md5) ? args.md5 : MD5(viewHash), _historyItem;
                            viewInstance.set('md5', md5);
                            /*
                             _historyItem = historyStore.get(md5)
                             || {md5: md5, anchor: requireUrl, hash: viewHash, routeTargetId: routeTargetId, iconClass: viewInstance.get('iconClass'), label: viewInstance.get('label'), position: (new Date()).getTime()};
                             */
                            _historyItem = {md5: md5, anchor: requireUrl, hash: viewHash, routeTargetId: routeTargetId, iconClass: viewInstance.get('iconClass'), label: viewInstance.get('label'), position: (new Date()).getTime(), widgetId: viewInstance.get('id')};
                            _historyItem.position = (new Date()).getTime();
                            _historyItem.hash = viewHash;
                            //    historyStore.put(_historyItem); moved into history.store

                            topic.publish('/route/history-activated', _historyItem);

                        };

                        var _initialization = function (viewInstance, doNotAddInHistory) {
                            //router_standby.show();
                            viewInstance.set('anchor', requireUrl);
                            viewInstance.set('hash', viewHash);
                            viewInstance.set('routeTargetId', routeTargetId);
                            viewInstance.set('routeModule', module);
                            viewInstance.set('routeController', controller);
                            viewInstance.set('routeView', view);
                            viewInstance.set('routeArgs', args || {});
                            //      console.log(viewInstance.activate.toString());
                            viewInstance.activate(args).then(_afterActivation(viewInstance, doNotAddInHistory));
                        };

                        if (existing.length > 0 && existing[0].get('onlyOneInstance')) {
                            if (target instanceof Dialog) {
                                target.show().then(_initialization(existing[0], true));
                            } else {
                                target.selectChild(existing[0]).then(_initialization(existing[0], false));
                                if (typeof existing[0].resize === 'function')
                                    target.resize();
                            }
                        } else {
                            var loadModuleErrorCallBack = require.on('error', function (e) {
                                loadModuleErrorCallBack.remove();
                                //                            logger.error("ROUTE FAILED " + requireUrl);
                                console.error(e);
                                var err = new Error("Can not load module: " + e.info[0]);

                                on.emit(target.domNode, "doze-error", {
                                    error: err,
                                    cancelable: true,
                                    bubbles: true
                                });
                            });


                            require(
                                    [
                                        module + '/module'
                                    ], function (Module) {
                                require([
                                    requireUrl
                                ], function (View) {
                                    loadModuleErrorCallBack.remove();
                                    var child = new View();
                                    _views.push(child.id);

                                    if (target instanceof Dialog) {
                                        target.destroyDescendants();
                                        target.addChild(child);
                                        target.show().then(_initialization(child, true));
                                    } else {
                                        target.addChild(child);
                                        target.selectChild(child).then(_initialization(child, false));
                                        if (typeof child.resize === 'function')
                                            target.resize();
                                    }
                                });
                            });
                        }
                    };
            var _changeLabel = function (view) {
                topic.publish('/route/history-changelabel', {md5: view.md5, label: view.get('label')});
//                historyStore.changeLabel(view.md5, view.get('label')); move to history.list

            };
            var currentPath = '';
            /*
             var _safeDialogClose = false;
             var _safeDialogOpen = false;
             */
            router.register(".*", function (evt) {
                var matches;
                var path = decodeURIComponent(evt.newPath);
                var oldpath = decodeURIComponent(evt.oldPath);

                var oldtarget = '';
                var r = lang.clone(regexpView);
                while ((matches = r.exec(oldpath)) !== null) {
                    oldtarget = matches[1] || 'centercontent';
                    matches[1] = oldtarget;
                }
                oldtarget = oldtarget.replace('/', '');

                var newtarget = '';
                while ((matches = r.exec(path)) !== null) {
                    newtarget = matches[1] || 'centercontent';
                    matches[1] = newtarget;
                }
                newtarget = newtarget.replace('/', '');

                if (oldtarget === 'popup') {
                    // close and destroy the popup + remove the url from the history
                    if (registry.byId(oldtarget) && registry.byId(oldtarget) instanceof Dialog) {
                        //_safeDialogClose = true;
                        var _children = registry.byId(oldtarget).getChildren();
                        if (_children && _children.length === 1) {
                            if (typeof _children[0].onClose === 'function') {
                                _children[0].onClose();
                            }
                        }
                        registry.byId(oldtarget).destroyDescendants();
                        registry.byId(oldtarget).destroy();
                    }
                }
                if ((oldtarget === newtarget) && /*_safeDialogOpen !== true && _safeDialogClose !== true &&*/
                        (currentPath !== evt.newPath)  // avoid cycling when desactivate failed

                        // call deactivate the old widget
                        && array.filter(_views, function (v) {
                            if (registry.byId(v) && typeof registry.byId(v).desactivate === 'function') {
                                return !!!registry.byId(v).desactivate();
                            }
                        }).length > 0) {
                         
                            
                    // deactivate failed -> return the old url
                    currentPath = evt.oldPath;


                    setTimeout(function () {
                        hash(evt.oldPath, true)
                    }, 100);


                    return;
                }
                topic.publish('/route/history-desactivated', true);
                /*
                 _safeDialogClose = false;
                 _safeDialogOpen = false;
                 */
                currentPath = '';


                var r = lang.clone(regexpView);
                var matches;
                while ((matches = r.exec(path)) !== null)
                {
                    //logger.info("ROUTE MODULE: " + matches[0]);
                    var viewHash = matches[0],
                            target = matches[1],
                            module = matches[2],
                            controller = matches[3],
                            view = matches[4],
                            params = matches[5];
                    loadView(encodeURI(viewHash), module, controller, view, params, target);
                }
            });
            var changeHash = function (view, args, replace, unsetArgs) {
                var viewHash = view.get('hash');
                var currentHash = hash();
                currentHash = decodeURIComponent(currentHash);
                var r = lang.clone(regexpView);

                var matches = r.exec(decodeURIComponent(viewHash));
                var oldArgs = json.fromJson(matches[5] || "{}");
                var params = lang.mixin(oldArgs, args);
                for (var prop in (unsetArgs || {})) {
                    delete params[prop];
                }
                var newHash = '';

                if (/({[^}]*})/.test(viewHash)) {
                    newHash = viewHash.replace(/({[^}]*})/, json.toJson(params));
                } else {
                    newHash = viewHash + json.toJson(params);
                }
                var redirection = currentHash.replace(viewHash, newHash);
                hash(encodeURI(redirection), replace);
                view.set('hash', redirection);
            };
            var _assemble = function (view, controller, module, args, targetId) {
                targetId = targetId || 'centercontent';
                return targetId + '/' + module + '/' + controller + '/' + view + json.toJson(args);
            };

            var _getHashList = function () {
                var currentHash = decodeURIComponent(hash());
                var hashList = {};
                var matches;
                var r = regexpView;
                while ((matches = r.exec(currentHash)) !== null)
                {
                    var targetWidget = matches[1] || 'centercontent';
                    targetWidget = targetWidget.replace('/', '');
                    hashList[targetWidget || 'centercontent'] = matches[0];
                }
                return hashList;
            };
            var _setHash = function (hashList, redirection) {
                //  hash(encodeURIComponent(_getFinalHash(hashList)), redirection);
                hash(_getFinalHash(hashList), redirection);
            };
            var _getFinalHash = function (hashList) {
                var finalHash = '';
                for (var h in hashList) {
                    finalHash += ((finalHash === '') ? '' : '|') + hashList[h];
                }
                return finalHash;
            };

            var _openURL = function (view, controller, module, args, targetId) {
                targetId = targetId || 'centercontent';
                var newHash = _assemble(view, controller, module, args, targetId);
                var hashList = _getHashList();
                hashList[targetId] = newHash;

                return encodeURI(_getFinalHash(hashList));
//            _setHash(hashList, redirection);
            };


            var _open = function (view, controller, module, args, targetId, redirection) {
                targetId = targetId || 'centercontent';
                if (!registry.byId(targetId)) {
                    _safeDialogOpen = true;
                }
                hash(_openURL(view, controller, module, args, targetId), redirection);
            };

            var _close = function (targetId) {
                /*
                 var hashList = _getHashList();
                 if (registry.byId(targetId) && registry.byId(targetId) instanceof Dialog) {
                 _safeDialogClose = true;
                 var _children = registry.byId(targetId).getChildren();
                 if (_children && _children.length === 1) {
                 if (typeof _children[0].onClose === 'function') {
                 _children[0].onClose();
                 }
                 }
                 delete hashList[targetId];
                 registry.byId(targetId).destroyDescendants();
                 registry.byId(targetId).destroy();
                 }
                 _setHash(hashList, false);
                 */
                var hashList = _getHashList();
                if (registry.byId(targetId) && registry.byId(targetId) instanceof Dialog) {
                    delete hashList[targetId];
                }
                _setHash(hashList, false);
            };

            return{
                regExp: regexpView,
                change: changeHash,
                load: loadView,
                open: _open,
                close: _close,
                assemble: _assemble,
                openURL: _openURL,
                changeLabel: _changeLabel
            };
        });