define(
        [
            "dojo/_base/declare",
            "dojo/on",
            "dojo/topic",
            "dojo/router",
            "dojo/dom-construct",
            "dojo/dom-class",
            "dijit/_Widget",
            "dijit/layout/_LayoutWidget",
            "dijit/_TemplatedMixin",
            "dijit/_WidgetsInTemplateMixin",
            "dojo/Evented",
            "dojo/text!./templates/title.html"

        ],
        function (
                declare, on, topic, router, domConstruct, domClass,
                _Widget, _LayoutWidget, _TemplatedMixin, _WidgetsInTemplateMixin, Evented, template,
                DijitRegistry, Grid
                ) {


            return declare("doze.router.history.title", [_Widget, _LayoutWidget, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
                templateString: template,
                label: '',
                baseClass: 'historyContainer',
                iconClass: '',
                _currentItemToRemove: null,
                postCreate: function () {
                    this.inherited(arguments);
                    this.titleNode.innerHTML = 'Starting...';
                    var _this = this;

                    topic.subscribe('/route/history-activated', function (item) {
                        this.iconClass = item.iconClass;
                        _this.titleNode.innerHTML = '&nbsp;<span class="glyph ' + this.iconClass + '" >&nbsp;</span>&nbsp;<span>' + item.label + '</span>';
                    });
                    topic.subscribe('/route/history-changelabel', function (item) {
                        _this.titleNode.innerHTML = '&nbsp;<span class="glyph ' + this.iconClass + '" >&nbsp;</span>&nbsp;<span>' + item.label + '</span>';

                    });
                }
            });
        });