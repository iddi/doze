define(
        [
            "dojo/_base/declare",
            "dojo/on",
            "dojo/topic",
            "dojo/router",
            "dojo/dom-construct",
            "dojo/dom-class",
            "dijit/_Widget",
            "dijit/layout/_LayoutWidget",
            "dijit/_TemplatedMixin",
            "dijit/_WidgetsInTemplateMixin",
            "dojo/Evented",
            "dojo/text!./templates/list.html",
            "dgrid/extensions/DijitRegistry",
            "dgrid/OnDemandGrid",
            "./store"
        ],
        function (
                declare, on, topic, router, domConstruct, domClass,
                _Widget, _LayoutWidget, _TemplatedMixin, _WidgetsInTemplateMixin, Evented, template,
                DijitRegistry, Grid, historyStore
                ) {

            var HistoryGridClass = declare("doze.router.history._defaultgrid", [Grid, DijitRegistry], {});

            return declare("doze.router.history.list", [_Widget, _LayoutWidget, _TemplatedMixin, _WidgetsInTemplateMixin, Evented], {
                templateString: template,
                label: '',
                baseClass: 'historyContainer',
                _currentItemToRemove: null,
                postCreate: function () {
                    this.inherited(arguments);
                    this.createGrid();
                    var _this = this;
                    on(this.searchNode, 'change', function (query) {
                        var _search = new RegExp(query, 'i');
                        _this.grid.set('query', function (obj) {
                            return _search.test(obj.label);
                        });

                    });
                    this.grid.on(".dgrid-cell:click", function (evt) {
                        var cell = _this.grid.cell(evt);
                        var _name = cell.column.id;
                        var _object = cell.row.data;
                        if (_name === 'label') {
                            router.go(_object.hash);
                        }
                        if (_name === 'button') {
                            var _items = _this.grid.store.query({}, {sort: [{attribute: 'position', descending: true}]});
                            if (_items[0].md5 === _object.md5) {
                                // on doit attendre que la vue actuelle soit désactivée -> dojo topic
                                _this._currentItemToRemove = _object.md5;
                                if (_items.length === 1) {
                                    router.go('centercontent/do-ze-admin-module/index/index');
                                } else {
                                    router.go(_items[1].hash + '|_' + (new Date()).getTime());
                                }
                            } else {
                                _this.grid.store.remove(_object.md5);
                            }
                            if (_items.length === 0) {
                                router.go('centercontent/do-ze-admin-module/index/index');
                            }
                        }
                    });
                    topic.subscribe('/route/history-desactivated', function () {
                        if (_this._currentItemToRemove) {
                            _this.grid.store.remove(_this._currentItemToRemove);
                        }
                        _this._currentItemToRemove = null;
                    });
                    topic.subscribe('/route/history-activated', function (historyItem) {
                        var _historyItem;
                        _historyItem = historyStore.get(historyItem.md5) || historyItem;

                        // update if history item already exists
                        _historyItem.position = historyItem.position;
                        _historyItem.hash = historyItem.hash;

                        historyStore.put(_historyItem);
                    });
                    topic.subscribe('/route/history-changelabel', function (historyItem) {
                        historyStore.changeLabel(historyItem.md5, historyItem.label);
                    });
                },
                createGrid: function () {
                    if (this.grid) {
                        return;
                    }
                    var gridArgs = {
                        region: "center",
                        columns: {
                            icon: {
                                field: "iconClass",
                                width: "22px",
                                renderCell: function (item, value, node, results) {

                                    return domConstruct.create('span', {innerHTML: '', "class": item.iconClass, title: item.label});
                                }
                            },
                            label: {field: "label"},
                            button: {
                                field: "md5",
                                width: "22px",
                                renderCell: function (item, value, node, results) {
                                    return domConstruct.create('span', {innerHTML: '', "class": 'icon-dozeminus', title: 'Remove from history'});
                                }
                            }
                        },
                        showHeader: false,
                        store: historyStore,
                        query: {},
                        queryOptions: {sort: [{attribute: 'position', descending: true}]}
                    };
                    this.grid = new HistoryGridClass(gridArgs, this.gridNode);
                }
            });
        });