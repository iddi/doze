define([
    "dojo/store/Observable",
    "dojo/store/Memory"
], function (Observable, Memory, domConstruct) {
    var _historyStore = Observable(new Memory(
            {
                data: [],
                idProperty: "md5"
            }));
    _historyStore.changeLabel = function (md5, label) {
        if (md5 && label && this.get(md5)) {
            var _item = this.get(md5);
            _item.label = label;
            this.put(_item);
        }
    };
    return _historyStore;

});