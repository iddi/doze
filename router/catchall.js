define(
        [
            "dojo/has",
            "dojo/router"
        ],
        function (has, router) {
            router.register(".*", function (evt) {
                console.log("ROUTE CATCHALL: " + decodeURIComponent(evt.newPath));
            });
        });

