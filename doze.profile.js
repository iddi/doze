var miniExcludes = {
    "put-selector/README.md": 1,
    "put-selector/package": 1
},
amdExcludes = {
},
        isTestRe = /\/test\//,
        copies = {
            "doze/util/build/plugins/template": 1,
            "doze/util/build/plugins/definition": 1
        };

var profile = {
    resourceTags: {
        test: function (filename, mid) {
            return isTestRe.test(filename);
        },
        miniExclude: function (filename, mid) {
            return isTestRe.test(filename) || mid in miniExcludes;
        },
        amd: function (filename, mid) {
            return /\.js$/.test(filename) && !(mid in amdExcludes);
        },
        copyOnly: function (filename, mid) {
            return (mid in copies);
        }
    }
};